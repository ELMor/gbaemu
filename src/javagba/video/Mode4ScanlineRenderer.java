package javagba.video;

import javagba.memory.Memory;
import javagba.memory.MemoryRAM;

public class Mode4ScanlineRenderer extends AbstractScanlineRenderer {

	public final static int PAGE0_ADDR = 0x0000000; // Offset of first page

	public final static int PAGE1_ADDR = 0x000a000; // Offset of second page

	public Mode4ScanlineRenderer(Renderer renderer) {

		// Call inherited constructor
		super(renderer);
	}

	public void renderScanline(int index) {

		// Initialise locals
		int pageOffset = PAGE0_ADDR;
		short pixel;
		int offset = index * Renderer.RES_X_SCREEN;
		MemoryRAM vramBank = (MemoryRAM) renderer.memory.banks[Memory.MEM_VRAM];
		MemoryRAM paletteBank = (MemoryRAM) renderer.memory.banks[Memory.MEM_PALETTE];

		// Get page offset
		if ((renderer.registers.getRegister(REG_DISPCNT) & DISPCNT_DISPLAYFRAME_F) != 0)
			pageOffset = PAGE1_ADDR;

		// Get pixel data reference
		final int[] pixels = renderer.getPixels();

		for (int x = 0; x < Renderer.RES_X_SCREEN; x++, offset++) {

			// Get index
			int pixelIndex = (0xff & vramBank.readByte(offset + pageOffset)) << 1;

			// Get color
			pixel = paletteBank.readHalfWord(pixelIndex);

			// Get shifted RGB components
			int r = (pixel & MASK_BLU) << 19;
			int g = (pixel & MASK_GRN) << 6;
			int b = (pixel & MASK_RED) >>> 7;

			// Store pixel
			pixels[offset] = 0xff000000 | r | g | b;
		}
	}
}
