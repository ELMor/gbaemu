package javagba.memory;

public class MemoryRAM implements AbstractMemory {

	public byte[] data; // Memory data

	public int mask; // Memory size

	public MemoryRAM(int size) {

		// Initialise data
		this.mask = size - 1;
		this.data = new byte[size];
	}

	public byte readByte(int address) {

		// Read byte
		return data[address & mask];
	}

	public short readHalfWord(int address) {

		// Return halfword
		return (short) ((0xff & readByte(address + 1)) << 8 | (0xff & readByte(address)));
	}

	public int readWord(int address) {

		// Return word
		return (0xff & readByte(address + 3)) << 24
				| (0xff & readByte(address + 2)) << 16
				| (0xff & readByte(address + 1)) << 8
				| (0xff & readByte(address));
	}

	public void writeByte(byte value, int address) {

		// Write byte to memory
		data[address & mask] = value;
	}

	public void writeHalfWord(short value, int address) {

		// Write halfword
		writeByte((byte) value, address);
		writeByte((byte) (value >>> 8), address + 1);
	}

	public void writeWord(int value, int address) {

		// Write word
		writeByte((byte) value, address);
		writeByte((byte) (value >>> 8), address + 1);
		writeByte((byte) (value >>> 16), address + 2);
		writeByte((byte) (value >>> 24), address + 3);
	}
}
