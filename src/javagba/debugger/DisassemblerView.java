package javagba.debugger;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Panel;
import java.awt.SystemColor;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javagba.core.Core;
import javagba.memory.Memory;

public class DisassemblerView extends Panel implements MouseListener {

	public final static int ITEM_COUNT = 23; // Number of items

	public final static int ITEM_HEIGHT = 16; // Height of single item in
												// pixels

	public Image offImage; // Offscreen image

	public Graphics offGraphics; // Offscreen graphics

	public DisassemblerItem[] items; // Visible items

	public Debugger owner; // Owner reference

	public Core core; // Core reference

	public Memory memory; // Memory reference

	protected DisassemblerView(Debugger owner, Core core, Memory memory,
			int width, int height) {

		// Call inherited constructor
		super();

		// Set layout
		setSize(width, height);
		setFont(new Font("Courier", Font.PLAIN, 12));

		// Initialise fields
		this.items = new DisassemblerItem[ITEM_COUNT];

		this.owner = owner;
		this.memory = memory;
		this.core = core;

		// Initialise items
		updateItems(0x08000000);
		addMouseListener(this);
	}

	public void mouseClicked(MouseEvent e) {

		int MODE_ARM = 0;
		int MODE_THUMB = 1;
		int mode = (core.getRegister(Core.REG_CPSR) & Core.PSR_FLAG_STATE) != 0 ? 1
				: 0;

		// Check for breakpoint clicks
		if (e.getX() <= 20 && e.getY() > 20) {

			int address = Integer.parseInt(items[0].address.substring(2), 16)
					+ (((e.getY() - 20) / ITEM_HEIGHT) * (mode == MODE_THUMB ? 2
							: 4));

			if (owner.isBreakpoint(address))
				owner.removeBreakpoint(address);
			else
				owner.addBreakpoint(address);

			updateItems(Integer.parseInt(items[0].address.substring(2), 16));
			repaint();
		}
	}

	/**
	 * mouseEntered method comment.
	 */
	public void mouseEntered(java.awt.event.MouseEvent e) {
	}

	/**
	 * mouseExited method comment.
	 */
	public void mouseExited(java.awt.event.MouseEvent e) {
	}

	/**
	 * mousePressed method comment.
	 */
	public void mousePressed(java.awt.event.MouseEvent e) {
	}

	/**
	 * mouseReleased method comment.
	 */
	public void mouseReleased(java.awt.event.MouseEvent e) {
	}

	public void paint(Graphics g) {

		// Get size
		Dimension size = getSize();

		// Fill background
		g.setColor(SystemColor.window);
		g.fillRect(0, 0, size.width, size.height);

		// Render edge
		g.setColor(SystemColor.controlShadow);
		g.drawRect(0, 0, size.width - 1, size.height - 1);
		g.setColor(SystemColor.controlDkShadow);
		g.drawRect(1, 1, size.width - 3, size.height - 3);
		g.setColor(SystemColor.controlHighlight);
		g.drawLine(size.width - 1, 0, size.width - 1, size.height);
		g.drawLine(0, size.height - 1, size.width - 1, size.height - 1);
		g.setColor(SystemColor.controlLtHighlight);
		g.drawLine(size.width - 2, 1, size.width - 2, size.height - 2);
		g.drawLine(1, size.height - 2, size.width - 3, size.height - 2);

		// Set clip
		g.setClip(2, 2, size.width - 4, size.height - 4);

		// Render margins
		g.setColor(SystemColor.control);
		g.fillRect(2, 2, 20, size.height - 4);
		g.fillRect(2, 2, size.width - 2, 20);
		g.setColor(SystemColor.controlShadow);
		g.drawLine(22, 2, 22, size.height - 4);
		g.drawLine(22, 21, size.width - 2, 21);

		// Render labels
		g.setColor(SystemColor.controlText);
		g.drawString("ADDRESS", 24, 16);
		g.drawString("INSTR", 104, 16);
		g.drawString("OPCODE", 184, 16);
		g.drawString("PARAMETERS", 264, 16);

		// Render seperators
		g.setColor(SystemColor.controlShadow);
		g.drawLine(100, 22, 100, size.height);
		g.drawLine(180, 22, 180, size.height);

		// Render items
		if (items[0] != null)
			for (int i = 0, offset = 22; i < ITEM_COUNT; i++, offset += ITEM_HEIGHT) {

				// Render cursor if needed
				if (items[i].isCursor()) {

					g.setColor(Color.magenta);
					g.fillRect(5, offset + 3, 15, ITEM_HEIGHT - 7);
					g.draw3DRect(5, offset + 3, 14, ITEM_HEIGHT - 8, true);
				}

				// Render breakpoint if needed
				if (items[i].isBreakpoint()) {

					g.setColor(Color.red);
					g.fillOval(5, offset + 3, ITEM_HEIGHT - 7, ITEM_HEIGHT - 7);
					g.setColor(Color.black);
					g.drawOval(5, offset + 3, ITEM_HEIGHT - 7, ITEM_HEIGHT - 7);
				}

				// Render address
				g.setColor(SystemColor.controlShadow);
				g.drawString(items[i].address, 24, offset + 12);
				g.drawString(items[i].instruction, 104, offset + 12);
				g.setColor(SystemColor.controlText);
				String repr=items[i].opcode==null?"���???":items[i].opcode;
				g.drawString(repr, 184, offset + 12);
				g.setColor(SystemColor.blue);
				
				g.drawString(items[i].parameters==null?"��??":items[i].parameters, 
						264, offset + 12);
			}
	}

	public void update(Graphics g) {

		// Create offscreen
		if (offGraphics == null) {

			offImage = createImage(getSize().width, getSize().height);
			offGraphics = offImage.getGraphics();
		}

		// Get size
		paint(offGraphics);

		// Blit
		g.drawImage(offImage, 0, 0, this);
	}

	public void updateItems() {

		int MODE_ARM = 0;
		int MODE_THUMB = 1;
		int mode = (core.getRegister(Core.REG_CPSR) & Core.PSR_FLAG_STATE) != 0 ? 1
				: 0;

		// Get update address
		int topAddress = Math.max(0, core.getRegister(Core.REG_PC)
				- (ITEM_COUNT / 2 * (mode == MODE_THUMB ? 2 : 4)));

		updateItems(topAddress);
	}

	public void updateItems(int topAddress) {

		int MODE_ARM = 0;
		int MODE_THUMB = 1;
		int mode = (core.getRegister(Core.REG_CPSR) & Core.PSR_FLAG_STATE) != 0 ? 1
				: 0;

		// Update items
		for (int i = 0; i < ITEM_COUNT; i++, topAddress += mode == MODE_THUMB ? 2
				: 4) {
			try {
				items[i] = new DisassemblerItem(
						mode == MODE_THUMB, 
						topAddress,
						mode == MODE_THUMB ? 
								(0xffff & memory.readHalfWord(topAddress)) : 
									      memory.readWord(topAddress));
				items[i].setCursor(core.getRegister(Core.REG_PC) == topAddress);
				items[i].setBreakpoint(owner.isBreakpoint(topAddress));
			} catch (Exception e) {
			}
		}

		// Repaint
		repaint();
	}
}
