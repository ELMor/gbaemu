package javagba.memory;

public class NonExistentMemory implements AbstractMemory {
	public int bankNumber=-1;
	
	public void out(String msg){
		System.out.println("Banco no existente #"+bankNumber+" "+msg);
	}
	
	public NonExistentMemory(int banknumber){
		this.bankNumber=banknumber;
	}
	
	public byte readByte(int address) {
		out("Leyendo byte 0x"+Integer.toHexString(address));
		return 0;
	}

	public short readHalfWord(int address) {
		out("Leyendo halfWord de 0x"+Integer.toHexString(address));
		return 0;
	}

	public int readWord(int address) {
		out("Leyendo Word 0x"+Integer.toHexString(address));
		return 0;
	}

	public void writeByte(byte value, int address) {
		out("Escribiendo byte "+value+" en 0x"+Integer.toHexString(address));
	}

	public void writeHalfWord(short value, int address) {
		out("Escribiendo halfword  "+value+" en 0x"+Integer.toHexString(address));
	}

	public void writeWord(int value, int address) {
		out("Escribiendo word "+value+" en 0x"+Integer.toHexString(address));
	}

}
