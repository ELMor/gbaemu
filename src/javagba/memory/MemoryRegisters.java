package javagba.memory;

import javagba.IORegConstants;

public class MemoryRegisters extends MemoryRAM implements IORegConstants {

	public Memory owner; // Owner reference

	public MemoryRegisters(Memory owner) {

		// Call inherited constructor
		super(0x02000);

		// Assign owner
		this.owner = owner;
	}

	public void generateInterrupt(int interruptBit) {

		// Only generate interrupt if master flag is set
		if ((getRegister(REG_IRQ_MASTER) & 0x0001) != 0) {

			// Make sure interrupt is enabled
			if ((getRegister(REG_IRQ_ENABLE) & interruptBit) != 0)
				setRegister(
						(short) (getRegister(REG_IRQ_REQUEST) | interruptBit),
						REG_IRQ_REQUEST);
		}
	}

	public int getRegister(int address) {

		// Return register value
		return (0xffff & readHalfWord(address));
	}

	public byte readByte(int address) {

		// Call inherited method
		return super.readByte(address);
	}

	public short readHalfWord(int address) {

		// Call inherited method
		return super.readHalfWord(address);
	}

	public int readWord(int address) {

		// Call inherited method
		return super.readWord(address);
	}

	public void setRegister(short value, int address) {

		// Set register value
		writeHalfWord(value, address);
	}

	public void writeByte(byte value, int address) {

		// Call inherited method
		super.writeByte(value, address);
	}

	public void writeHalfWord(short value, int address) {

		// Call inherited method
		super.writeHalfWord(value, address);

		// Handle DMA related writes
		if (address >= REG_DM0SAD_L && address <= REG_DM3CNT_H)
			owner.dma.handleRegisterWrite(value, address);

		// Handle key writes
		if (address == REG_P1)
			if ((getRegister(REG_P1CNT) & P1CNT_ENABLE) != 0) {

				// Handle OR operation
				if ((getRegister(REG_P1CNT) & P1CNT_INTCOND) != 0) {
					if (((getRegister(REG_P1) & 0x03ff) & (getRegister(REG_P1CNT) & 0x03ff)) != 0)
						generateInterrupt(IRQ_KEY);
				} else {
					if (((getRegister(REG_P1) & 0x03ff) | (getRegister(REG_P1CNT) & 0x03ff)) != 0)
						generateInterrupt(IRQ_KEY);
				}
			}
	}

	public void writeWord(int value, int address) {

		// Call inherited method
		super.writeWord(value, address);
	}
}
