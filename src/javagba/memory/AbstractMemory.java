package javagba.memory;

public interface AbstractMemory {
	public byte readByte(int address);

	public short readHalfWord(int address);

	public int readWord(int address);

	void writeByte(byte value, int address);

	void writeHalfWord(short value, int address);

	void writeWord(int value, int address);
}
