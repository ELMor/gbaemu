package javagba.video;

import javagba.memory.Memory;
import javagba.memory.MemoryRAM;

public class Mode5ScanlineRenderer extends AbstractScanlineRenderer {

	public final static int PAGE0_ADDR = 0x0000000; // Offset of first page

	public final static int PAGE1_ADDR = 0x000a000; // Offset of second page

	public Mode5ScanlineRenderer(Renderer renderer) {

		// Call inherited constructor
		super(renderer);
	}

	public void renderScanline(int index) {

		// Ignore scanline not visible in this mode
		if (index >= 128)
			return;

		// Initialise page offset
		int pageOffset = PAGE0_ADDR;

		// Get page offset
		if ((renderer.registers.getRegister(REG_DISPCNT) & DISPCNT_DISPLAYFRAME_F) != 0)
			pageOffset = PAGE1_ADDR;

		// Initialise locals
		int pixel;
		int offset = index * Renderer.RES_X_SCREEN;
		int pixOffset = index * 160;
		MemoryRAM bank = (MemoryRAM) renderer.memory.banks[Memory.MEM_VRAM];

		// Get pixel data reference
		final int[] pixels = renderer.getPixels();

		for (int x = 0; x < 160; x++, offset++, pixOffset++) {

			// Get VRAM data
			pixel = bank.readHalfWord((pixOffset << 1) + pageOffset);

			// Get shifted RGB components
			int r = (pixel & MASK_BLU) << 19;
			int g = (pixel & MASK_GRN) << 6;
			int b = (pixel & MASK_RED) >>> 7;

			// Store pixel
			pixels[offset] = 0xff000000 | r | g | b;
		}
	}
}
