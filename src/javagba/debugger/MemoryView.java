package javagba.debugger;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Panel;
import java.awt.SystemColor;

public class MemoryView extends Panel {

	public final static int ITEM_COUNT = 9; // Number of items

	public final static int ITEM_HEIGHT = 16; // Height of single item in
												// pixels

	public Image offImage; // Offscreen image

	public Graphics offGraphics; // Offscreen graphics

	public Debugger owner; // Owner reference

	public DebugMemory memory; // Memory reference

	public String[] items;

	protected MemoryView(Debugger owner, DebugMemory memory, int width,
			int height) {

		// Call inherited constructor
		super();

		// Set layout
		setSize(width, height);
		setFont(new Font("Courier", Font.BOLD, 12));

		// Initialise fields
		this.owner = owner;
		this.memory = memory;

		// Initialise items
		this.items = new String[ITEM_COUNT];
		updateItems(0x08000000);
	}

	public void paint(Graphics g) {

		// Get size
		Dimension size = getSize();

		// Fill background
		g.setColor(SystemColor.window);
		g.fillRect(0, 0, size.width, size.height);

		// Render edge
		g.setColor(SystemColor.controlShadow);
		g.drawRect(0, 0, size.width - 1, size.height - 1);
		g.setColor(SystemColor.controlDkShadow);
		g.drawRect(1, 1, size.width - 3, size.height - 3);
		g.setColor(SystemColor.controlHighlight);
		g.drawLine(size.width - 1, 0, size.width - 1, size.height);
		g.drawLine(0, size.height - 1, size.width - 1, size.height - 1);
		g.setColor(SystemColor.controlLtHighlight);
		g.drawLine(size.width - 2, 1, size.width - 2, size.height - 2);
		g.drawLine(1, size.height - 2, size.width - 3, size.height - 2);

		// Set clip
		g.setClip(2, 2, size.width - 4, size.height - 4);

		// Render margins
		g.setColor(SystemColor.control);
		g.fillRect(2, 2, 20, size.height - 4);
		g.fillRect(2, 2, size.width - 2, 20);
		g.setColor(SystemColor.controlShadow);
		g.drawLine(22, 2, 22, size.height - 4);
		g.drawLine(22, 21, size.width - 2, 21);

		// Render labels
		g.setColor(SystemColor.controlText);
		g.drawString("ADDRESS", 24, 16);
		g.drawString("DATA", 104, 16);

		// Render seperators
		g.setColor(SystemColor.controlShadow);
		g.drawLine(100, 22, 100, size.height);

		for (int i = 0, offset = 22; i < ITEM_COUNT; i++, offset += 20) {

			// Render address
			g.setColor(SystemColor.controlText);
			g.drawString(items[i], 24, offset + 12);
		}
	}

	public static String toHexString(int value, int length) {

		String s = Integer.toHexString(value);

		if (length == 2 && s.length() > 2)
			s = s.substring(s.length() - 2);
		while (s.length() < length)
			s = "0" + s;

		return s;
	}

	public void update(Graphics g) {

		// Create offscreen
		if (offGraphics == null) {

			offImage = createImage(getSize().width, getSize().height);
			offGraphics = offImage.getGraphics();
		}

		// Get size
		paint(offGraphics);

		// Blit
		g.drawImage(offImage, 0, 0, this);
	}

	public void updateItems(int topAddress) {

		// topAddress -= 0x40;

		memory.setReportBack(false);

		// Update items
		for (int i = 0; i < ITEM_COUNT; i++) {

			items[i] = "0x" + toHexString(topAddress, 8) + "  ";

			for (int j = 0; j < 16; j++, topAddress++)
				items[i] += toHexString(memory.readByte(topAddress), 2) + " ";
		}
		memory.setReportBack(true);

		// Repaint
		repaint();
	}
}
