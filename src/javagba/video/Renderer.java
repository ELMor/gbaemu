package javagba.video;

import javagba.IORegConstants;
import javagba.memory.Memory;
import javagba.memory.MemoryRegisters;

public class Renderer implements IORegConstants {

	public final static int RES_X_SCREEN = 240; // Number of visible horizontal
												// pixels

	public final static int RES_Y_SCREEN = 160; // Number of visible vertical
												// pixels

	public final static int RES_X_TOTAL = 308; // Total number of horizontal
												// pixels

	public final static int RES_Y_TOTAL = 228; // Total number of vertical
												// pixels

	public final static double REFRESH_RATE = 59.727; // Refresh rate of
														// screen

	public final static double CLOCK_RATE = 16.78; // Core MHZ

	public final static int FRAMES_PER_SEC = 40; // FPS

	public static int CYCLES_HBLANK; // Number of cycles before HBLANK occurs

	public static int CYCLES_SCANLINE; // Number of cycles before scanline
										// completion

	public int[] pixels; // Pixel data reference

	public int frameIndex;

	public long frameTime;

	public int sleepTime = 0;

	public Memory memory; // Memory manager reference

	public MemoryRegisters registers; // Register manager reference

	public AbstractScanlineRenderer[] renderers; // Scanline renderers

	public int cyclesScanline; // Cycles passed since start of scanline render

	public int cyclesScreen; // Cycles passed since start of screen render

	public Viewport viewport; // Viewport reference

	public Renderer(Memory memory) {

		// Assign memory manager
		this.memory = memory;
		this.registers = (MemoryRegisters) memory.banks[Memory.MEM_IOREG];

		// Initialise renderers
		renderers = new AbstractScanlineRenderer[6];
		renderers[0] = new Mode0ScanlineRenderer(this);
		renderers[1] = new Mode1ScanlineRenderer(this);
		renderers[2] = new Mode2ScanlineRenderer(this);
		renderers[3] = new Mode3ScanlineRenderer(this);
		renderers[4] = new Mode4ScanlineRenderer(this);
		renderers[5] = new Mode5ScanlineRenderer(this);

		// Initialise constants
		init();
	}

	public void addCycles(int cycles) {

		// Add cycles to current scanline cycle counter
		cyclesScanline += cycles;

		// Get current scanline
		int vcount = registers.getRegister(REG_VCOUNT);

		// Get current LCD status
		int dispstat = registers.getRegister(REG_DISPSTAT);
		int dispcnt = registers.getRegister(REG_DISPCNT);

		// Get HBLANK and VBLANK flags
		boolean flagHBlank = cyclesScanline > CYCLES_HBLANK;
		boolean flagVBlank = vcount > RES_Y_SCREEN;

		// Render scanline if needed
		if (flagHBlank && (dispstat & DISPSTAT_HBLANK_EVAL_F) == 0) {

			// Get current render mode
			int mode = (dispcnt & DISPCNT_BGMODE_M) % 6;

			// Render scanline
			if (vcount < RES_Y_SCREEN)
				renderers[mode].renderScanline(vcount);

			// Generate HBLANK interrupt
			if ((dispstat & DISPSTAT_HBLANK_INT_F) != 0)
				registers.generateInterrupt(IRQ_HBLANK);
		}

		// Check if we should move to next scanline
		if (cyclesScanline > CYCLES_SCANLINE) {

			// Reset scanline cycle counter
			cyclesScanline -= CYCLES_SCANLINE;

			// Move to next scanline
			if (vcount < RES_Y_TOTAL - 1)
				registers.setRegister((short) (vcount + 1), REG_VCOUNT);
			else
				registers.setRegister((short) 0, REG_VCOUNT);

			// Repaint if screen is rendered
			if (vcount == RES_Y_SCREEN - 1) {

				// Refresh pixels and repaint viewport
				viewport.bufferSource.newPixels();
				viewport.repaint();

				frameIndex++;

				// Evaluate FPS every 8 frames
				if ((frameIndex & 7) == 0) {

					long delta = System.currentTimeMillis() - frameTime;
					long target = 1000 * 8 / FRAMES_PER_SEC;

					if (delta < target)
						sleepTime += 2;
					else if (delta > target)
						sleepTime -= 2;

					frameTime = System.currentTimeMillis();
				}
				try {
					Thread.sleep(sleepTime);
				} catch (Exception e) {
					// TODO: handle exception
				}
			}

			// Generate VBLANK if needed
			if (vcount == RES_Y_SCREEN)
				if ((dispstat & DISPSTAT_VBLANK_INT_F) != 0)
					registers.generateInterrupt(IRQ_VBLANK);
		}

		// Store flags
		dispstat = (dispstat & 0xfffc) | (flagHBlank ? 2 : 0)
				| (flagVBlank ? 1 : 0);

		// Store LCD status register
		registers.setRegister((short) dispstat, REG_DISPSTAT);

	}

	public int[] getPixels() {

		// Return pixel data reference
		return pixels;
	}

	private void init() {

		// Get number of cycles per screen update
		double cyclesPerUpdate = (CLOCK_RATE * 1000000) / REFRESH_RATE;

		// Get number of cycles per scanline
		double cyclesPerScanline = cyclesPerUpdate / RES_Y_TOTAL;

		// Initialise constants
		CYCLES_SCANLINE = (int) cyclesPerScanline;
		CYCLES_HBLANK = (int) (cyclesPerScanline * RES_X_SCREEN / RES_X_TOTAL);

		// Create buffer
		pixels = new int[240 * 160];
	}

	public void setViewport(Viewport newViewport) {

		// Assign viewport
		viewport = newViewport;
	}
}
