package javagba.video;

import java.awt.Dimension;
import java.awt.Event;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Panel;
import java.awt.SystemColor;
import java.awt.image.DirectColorModel;
import java.awt.image.MemoryImageSource;

import javagba.IORegConstants;

public class Viewport extends Panel implements IORegConstants {

	public Image offImage; // Offscreen image

	public Graphics offGraphics; // Offscreen graphics

	public MemoryImageSource bufferSource; // Buffer image producer reference

	public Image bufferImage; // Buffer memory image

	public int keyDownFlags; // Key down flags

	public Renderer renderer; // Renderer reference

	public Viewport(Renderer renderer) {

		// Initialise buffer
		// bufferSource = new MemoryImageSource(240, 160, new
		// DirectColorModel(15, 0x7c00, 0x03e0, 0x001f), renderer.getPixels(),
		// 0, 240);
		bufferSource = new MemoryImageSource(240, 160, new DirectColorModel(32,
				0xff0000, 0x00ff00, 0x0000ff), renderer.getPixels(), 0, 240);
		bufferSource.setAnimated(true);
		bufferSource.setFullBufferUpdates(false);

		// Create image
		bufferImage = createImage(bufferSource);

		// Assign renderer
		this.renderer = renderer;

		// Initialise key down flag array
		keyDownFlags = P1_ALL;
		updateKeyFlags();
	}

	public boolean keyDown(Event evt, int key) {

		// Handle key event
		switch (key) {

		// Handle key up
		case Event.UP:
			keyDownFlags &= ~P1_UP;
			break;
		case Event.DOWN:
			keyDownFlags &= ~P1_DOWN;
			break;
		case Event.LEFT:
			keyDownFlags &= ~P1_LEFT;
			break;
		case Event.RIGHT:
			keyDownFlags &= ~P1_RIGHT;
			break;
		case Event.ENTER:
			keyDownFlags &= ~P1_START;
			break;
		case ' ':
			keyDownFlags &= ~P1_SELECT;
			break;
		case 'a':
			keyDownFlags &= ~P1_L;
			break;
		case 's':
			keyDownFlags &= ~P1_R;
			break;
		case 'z':
			keyDownFlags &= ~P1_A;
			break;
		case 'x':
			keyDownFlags &= ~P1_B;
			break;
		}

		// Update flags
		updateKeyFlags();

		// Event was handled
		return true;
	}

	public boolean keyUp(Event evt, int key) {

		// Handle key event
		switch (key) {

		// Handle key up
		case Event.UP:
			keyDownFlags |= P1_UP;
			break;
		case Event.DOWN:
			keyDownFlags |= P1_DOWN;
			break;
		case Event.LEFT:
			keyDownFlags |= P1_LEFT;
			break;
		case Event.RIGHT:
			keyDownFlags |= P1_RIGHT;
			break;
		case Event.ENTER:
			keyDownFlags |= P1_START;
			break;
		case ' ':
			keyDownFlags |= P1_SELECT;
			break;
		case 'a':
			keyDownFlags |= P1_L;
			break;
		case 's':
			keyDownFlags |= P1_R;
			break;
		case 'z':
			keyDownFlags |= P1_A;
			break;
		case 'x':
			keyDownFlags |= P1_B;
			break;
		}

		// Update flags
		updateKeyFlags();

		// Event was handled
		return true;
	}

	public void paint(Graphics g) {

		// Get size
		Dimension size = getSize();

		// Render edge
		g.setColor(SystemColor.controlShadow);
		g.drawRect(0, 0, size.width - 1, size.height - 1);
		g.setColor(SystemColor.controlDkShadow);
		g.drawRect(1, 1, size.width - 3, size.height - 3);
		g.setColor(SystemColor.controlHighlight);
		g.drawLine(size.width - 1, 0, size.width - 1, size.height);
		g.drawLine(0, size.height - 1, size.width - 1, size.height - 1);
		g.setColor(SystemColor.controlLtHighlight);
		g.drawLine(size.width - 2, 1, size.width - 2, size.height - 2);
		g.drawLine(1, size.height - 2, size.width - 3, size.height - 2);

		// Set clip
		g.setClip(2, 2, size.width - 4, size.height - 4);

		// Draw buffer
		g.drawImage(bufferImage, 2, 2, this);
	}

	public void update(Graphics g) {

		// Create offscreen
		if (offGraphics == null) {

			offImage = createImage(getSize().width, getSize().height);
			offGraphics = offImage.getGraphics();
		}

		// Get size
		paint(offGraphics);

		// Blit
		g.drawImage(offImage, 0, 0, this);
	}

	private void updateKeyFlags() {

		// Store keyflags
		renderer.registers.setRegister((short) keyDownFlags, REG_P1);
	}
}
