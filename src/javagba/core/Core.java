package javagba.core;

import javagba.CoreConstants;
import javagba.Eladio;
import javagba.memory.Memory;

public class Core implements CoreConstants, Runnable {		

	public final static boolean DEBUG = true;
	public final static int		INSTR_SIZE_ARM		= 4;			// Size of ARM state instruction in bytes
	public final static int		INSTR_SIZE_THUMB	= 2;			// Size of THUMB state instruction in bytes
	
	private Thread				thread;								// Core thread reference
	
	private boolean[]			conditionTable;						// Condition check lookup table
	private boolean				haltExecution;						// Halt execution flag
	
	public Memory				memory;								// Memory manager reference
	
	public int[][]				registers;							// Register banks
	public int[]				currentRegisters;					// Current list of registers
	public int					cycleCount;							// Current cycle counter

	
	
  static final public int nFlagBit  = 0x80000000; // negative or less than.
  static final public int zFlagBit  = 0x40000000; // zero.
  static final public int cFlagBit  = 0x20000000; // carry or borrow or extends.
  static final public int vFlagBit  = 0x10000000; // overflow.
  static final public int iFlagBit  = 0x00000080; // irq disable.
  static final public int fFlagBit  = 0x00000040; // fiq disable.
  static final public int thumbState  = 0x00000020; // (thumb) state bit.

  static final protected int cFlagBitNumber = 29; // the c-flag is the bit number 29.

  static final public int modeBitsMask = 0x0000001f; // Cover all bits concerning mode.
  static final public int usrModeBits  = 0x00000010; // User mode.
  static final public int fiqModeBits  = 0x00000011; // FIQ mode.
  static final public int irqModeBits  = 0x00000012; // IRQ mode.
  static final public int svcModeBits  = 0x00000013; // Supervisor mode.
  static final public int abtModeBits  = 0x00000017; // Abort mode.
  static final public int undModeBits  = 0x0000001b; // Undefined mode.
  static final public int sysModeBits  = 0x0000001f; // System mode.

  static final protected int resetVectorAddress                = 0x00000000;
  static final protected int undefinedInstructionVectorAddress = 0x00000004;
  static final protected int softwareInterrupVectorAddress     = 0x00000008;
  static final protected int prefetchAbortVectorAddress        = 0x0000000c;
  static final protected int dataAbortVectorAddress            = 0x00000010;
  static final protected int irqVectorAddress                  = 0x00000018;
  static final protected int fiqVectorAddress                  = 0x0000001c;

  private boolean stopPolitelyRequested = false;

  static final protected int armStateConditionBitsMask = 0xf0000000; // the bits concerning the condition in the instruction
  static final protected int armStateConditionEQBits   = 0x00000000; // equal
  static final protected int armStateConditionNEBits   = 0x10000000; // not equal
  static final protected int armStateConditionCSBits   = 0x20000000; // unsigned higher or same
  static final protected int armStateConditionCCBits   = 0x30000000; // unsigned lower
  static final protected int armStateConditionMIBits   = 0x40000000; // negative
  static final protected int armStateConditionPLBits   = 0x50000000; // positive or zero
  static final protected int armStateConditionVSBits   = 0x60000000; // overflow
  static final protected int armStateConditionVCBits   = 0x70000000; // no overflow
  static final protected int armStateConditionHIBits   = 0x80000000; // unsigned higher
  static final protected int armStateConditionLSBits   = 0x90000000; // unsigned lower or same
  static final protected int armStateConditionGEBits   = 0xa0000000; // greater or equal
  static final protected int armStateConditionLTBits   = 0xb0000000; // less than
  static final protected int armStateConditionGTBits   = 0xc0000000; // greater than
  static final protected int armStateConditionLEBits   = 0xd0000000; // less than or equal
  static final protected int armStateConditionALBits   = 0xe0000000; // always

  static final protected int armStateBXInstructionMask = 0x0ffffff0;
  static final protected int armStateBXInstructionBits = 0x012fff10;
  static final protected int armStateBXRnMask          = 0x0000000f;

  static final protected int armStateBInstructionMask = 0x0e000000;
  static final protected int armStateBInstructionBits = 0x0a000000;
  static final protected int armStateBLinkBit         = 0x01000000;
  static final protected int armStateBOffsetMask      = 0x00ffffff;

  static final protected int armStateDataProcessingInstructionMask = 0x0c000000;
  static final protected int armStateDataProcessingInstructionBits = 0x00000000;
  static final protected int armStateDataProcessingOpcodeMask      = 0x01e00000;
  static final protected int armStateDataProcessingANDBits         = 0x00000000;
  static final protected int armStateDataProcessingEORBits         = 0x00200000;
  static final protected int armStateDataProcessingSUBBits         = 0x00400000;
  static final protected int armStateDataProcessingRSBBits         = 0x00600000;
  static final protected int armStateDataProcessingADDBits         = 0x00800000;
  static final protected int armStateDataProcessingADCBits         = 0x00a00000;
  static final protected int armStateDataProcessingSBCBits         = 0x00c00000;
  static final protected int armStateDataProcessingRSCBits         = 0x00e00000;
  static final protected int armStateDataProcessingTSTBits         = 0x01000000;
  static final protected int armStateDataProcessingTEQBits         = 0x01200000;
  static final protected int armStateDataProcessingCMPBits         = 0x01400000;
  static final protected int armStateDataProcessingCMNBits         = 0x01600000;
  static final protected int armStateDataProcessingORRBits         = 0x01800000;
  static final protected int armStateDataProcessingMOVBits         = 0x01a00000;
  static final protected int armStateDataProcessingBICBits         = 0x01c00000;
  static final protected int armStateDataProcessingMVNBits         = 0x01e00000;
  static final protected int armStateDataProcessingImmediateBit    = 0x02000000;
  static final protected int armStateDataProcessingSetConditionBit = 0x00100000;
  static final protected int armStateDataProcessingOperand1Mask    = 0x000f0000;
  static final protected int armStateDataProcessingOperand2Mask    = 0x00000fff;
  static final protected int armStateDataProcessingDestinationMask = 0x0000f000;

  static final protected int armStateDataProcessingOp2RegisterMask   = 0x0000000f;
  static final protected int armStateDataProcessingShiftModeBit      = 0x00000010;
  static final protected int armStateDataProcessingShiftTypeMask     = 0x00000060;
  static final protected int armStateDataProcessingShiftAmountMask   = 0x00000f80;
  static final protected int armStateDataProcessingShiftRegisterMask = 0x00000f00;
  static final protected int armStateDataProcessingLogicalLeftBits   = 0x00000000;
  static final protected int armStateDataProcessingLogicalRightBits  = 0x00000020;
  static final protected int armStateDataProcessingArithmRightBits   = 0x00000040;
  static final protected int armStateDataProcessingRotateRightBits   = 0x00000060;

  static final protected int armStateDataProcessingOp2RotateMask    = 0x00000f00;
  static final protected int armStateDataProcessingOp2ImmediateMask = 0x000000ff;

  static final protected int armStateMRSInstructionMask  = 0x0fbf0fff;
  static final protected int armStateMRSInstructionBits  = 0x010f0000;
  static final protected int armStateMRSSourceBit        = 0x00400000;
  static final protected int armStateMRSDestinationMask  = 0x0000f000;

  static final protected int armStateMSR1InstructionMask = 0x0fbffff0;
  static final protected int armStateMSR1InstructionBits = 0x0129f000;
  static final protected int armStateMSR1SourceMask      = 0x0000000f;
  static final protected int armStateMSR1DestinationBit  = 0x00400000;
  static final protected int armStateMSR1FlagsOnly       = 0xf0000000;

  static final protected int armStateMSR2InstructionMask = 0x0dbff000;
  static final protected int armStateMSR2InstructionBits = 0x0128f000;
  static final protected int armStateMSR2SourceMask      = 0x0000000f;
  static final protected int armStateMSR2DestinationBit  = 0x00400000;
  static final protected int armStateMSR2ImmediateBit    = 0x02000000;
  static final protected int armStateMSR2ImmValueMask    = 0x000000ff;
  static final protected int armStateMSR2ImmRotateMask   = 0x00000f00;
  static final protected int armStateMSR2FlagsOnly       = 0xf0000000;

  static final protected int armStateMULInstructionMask = 0x0fc000f0;
  static final protected int armStateMULInstructionBits = 0x00000090;
  static final protected int armStateMULAccumulateBit   = 0x00200000;
  static final protected int armStateMULSetConditionBit = 0x00100000;
  static final protected int armStateMULRdMask          = 0x000f0000;
  static final protected int armStateMULRnMask          = 0x0000f000;
  static final protected int armStateMULRsMask          = 0x00000f00;
  static final protected int armStateMULRmMask          = 0x0000000f;

  static final protected int armStateMULLInstructionMask = 0x0f8000f0;
  static final protected int armStateMULLInstructionBits = 0x00800090;
  static final protected int armStateMULLUnsignedBit     = 0x00400000;
  static final protected int armStateMULLAccumulateBit   = 0x00200000;
  static final protected int armStateMULLSetConditionBit = 0x00100000;
  static final protected int armStateMULLRdHiMask        = 0x000f0000;
  static final protected int armStateMULLRdLoMask        = 0x0000f000;
  static final protected int armStateMULLRsMask          = 0x00000f00;
  static final protected int armStateMULLRmMask          = 0x0000000f;

  static final protected int armStateLDRSTRInstructionMask  = 0x0c000000;
  static final protected int armStateLDRSTRInstructionBits  = 0x04000000;
  static final protected int armStateLDRSTRImmediateBit     = 0x02000000;
  static final protected int armStateLDRSTRPreIndexingBit   = 0x01000000;
  static final protected int armStateLDRSTRUpDownBit        = 0x00800000;
  static final protected int armStateLDRSTRByteWordBit      = 0x00400000;
  static final protected int armStateLDRSTRWriteBackBit     = 0x00200000;
  static final protected int armStateLDRSTRLoadStoreBit     = 0x00100000;
  static final protected int armStateLDRSTRRnMask           = 0x000f0000;
  static final protected int armStateLDRSTRRdMask           = 0x0000f000;
  static final protected int armStateLDRSTRImmediateMask    = 0x00000fff;
  static final protected int armStateLDRSTRRmMask           = 0x0000000f;
  static final protected int armStateLDRSTRShiftTypeMask    = 0x00000060;
  static final protected int armStateLDRSTRShiftAmountMask  = 0x00000f80;
  static final protected int armStateLDRSTRLogicalLeftBits  = 0x00000000;
  static final protected int armStateLDRSTRLogicalRightBits = 0x00000020;
  static final protected int armStateLDRSTRArithmRightBits  = 0x00000040;
  static final protected int armStateLDRSTRRotateRightBits  = 0x00000060;

  static final protected int armStateHSDTROInstructionMask = 0x0e400f90;
  static final protected int armStateHSDTROInstructionBits = 0x00000090;
  static final protected int armStateHSDTROPreIndexingBit  = 0x01000000;
  static final protected int armStateHSDTROUpDownBit       = 0x00800000;
  static final protected int armStateHSDTROWriteBackBit    = 0x00200000;
  static final protected int armStateHSDTROLoadStoreBit    = 0x00100000;
  static final protected int armStateHSDTRORnMask          = 0x000f0000;
  static final protected int armStateHSDTRORdMask          = 0x0000f000;
  static final protected int armStateHSDTROSHMask          = 0x00000060;
  static final protected int armStateHSDTROSWPInstrBits    = 0x00000000;
  static final protected int armStateHSDTROUnsignHalfBits  = 0x00000020;
  static final protected int armStateHSDTROSignedByteBits  = 0x00000040;
  static final protected int armStateHSDTROSignedHalfBits  = 0x00000060;
  static final protected int armStateHSDTRORmMask          = 0x0000000f;

  static final protected int armStateHSDTIOInstructionMask = 0x0e400090;
  static final protected int armStateHSDTIOInstructionBits = 0x00400090;
  static final protected int armStateHSDTIOPreIndexingBit  = 0x01000000;
  static final protected int armStateHSDTIOUpDownBit       = 0x00800000;
  static final protected int armStateHSDTIOWriteBackBit    = 0x00200000;
  static final protected int armStateHSDTIOLoadStoreBit    = 0x00100000;
  static final protected int armStateHSDTIORnMask          = 0x000f0000;
  static final protected int armStateHSDTIORdMask          = 0x0000f000;
  static final protected int armStateHSDTIOHiOffsetMask    = 0x00000f00;
  static final protected int armStateHSDTIOSHMask          = 0x00000060;
  static final protected int armStateHSDTIOSWPInstrBits    = 0x00000000;
  static final protected int armStateHSDTIOUnsignHalfBits  = 0x00000020;
  static final protected int armStateHSDTIOSignedByteBits  = 0x00000040;
  static final protected int armStateHSDTIOSignedHalfBits  = 0x00000060;
  static final protected int armStateHSDTIOLoOffsetMask    = 0x0000000f;

  static final protected int armStateLDMSTMInstructionMask = 0x0e000000;
  static final protected int armStateLDMSTMInstructionBits = 0x08000000;
  static final protected int armStateLDMSTMPreIndexingBit  = 0x01000000;
  static final protected int armStateLDMSTMUpDownBit       = 0x00800000;
  static final protected int armStateLDMSTMPSRBit          = 0x00400000;
  static final protected int armStateLDMSTMWriteBackBit    = 0x00200000;
  static final protected int armStateLDMSTMLoadStoreBit    = 0x00100000;
  static final protected int armStateLDMSTMRnMask          = 0x000f0000;
  static final protected int armStateLDMSTMR15Bit          = 0x00008000;

  static final protected int armStateSWPInstructionMask = 0x0fb00ff0;
  static final protected int armStateSWPInstructionBits = 0x01000090;
  static final protected int armStateSWPByteWordBit     = 0x00400000;
  static final protected int armStateSWPRnMask          = 0x000f0000;
  static final protected int armStateSWPRdMask          = 0x0000f000;
  static final protected int armStateSWPRmMask          = 0x0000000f;

  static final protected int armStateSWIInstructionMask = 0x0f000000;
  static final protected int armStateSWIInstructionBits = 0x0f000000;

  static final protected int armStateUndefInstructionMask = 0x0e000010;
  static final protected int armStateUndefInstructionBits = 0x06000010;

  static final protected int thumbStateF1InstructionMask = 0xe000;
  static final protected int thumbStateF1InstructionBits = 0x0000;
  static final protected int thumbStateF1RsMask          = 0x0038;
  static final protected int thumbStateF1RdMask          = 0x0007;
  static final protected int thumbStateF1OpMask          = 0x1800;
  static final protected int thumbStateF1OffsetMask      = 0x07c0;
  static final protected int thumbStateF1LslBits         = 0x0000;
  static final protected int thumbStateF1LsrBits         = 0x0800;
  static final protected int thumbStateF1AsrBits         = 0x1000;

  static final protected int thumbStateF2InstructionMask = 0xf800;
  static final protected int thumbStateF2InstructionBits = 0x1800;
  static final protected int thumbStateF2RsMask          = 0x0038;
  static final protected int thumbStateF2RdMask          = 0x0007;
  static final protected int thumbStateF2RnMask          = 0x01c0;
  static final protected int thumbStateF2ImmediateBit    = 0x0400;
  static final protected int thumbStateF2OpBit           = 0x0200;

  static final protected int thumbStateF3InstructionMask = 0xe000;
  static final protected int thumbStateF3InstructionBits = 0x2000;
  static final protected int thumbStateF3OffsetMask      = 0x00ff;
  static final protected int thumbStateF3RdMask          = 0x0700;
  static final protected int thumbStateF3OpMask          = 0x1800;
  static final protected int thumbStateF3MovBits         = 0x0000;
  static final protected int thumbStateF3CmpBits         = 0x0800;
  static final protected int thumbStateF3AddBits         = 0x1000;
  static final protected int thumbStateF3SubBits         = 0x1800;

  static final protected int thumbStateF4InstructionMask = 0xfc00;
  static final protected int thumbStateF4InstructionBits = 0x4000;
  static final protected int thumbStateF4OpMask          = 0x03c0;
  static final protected int thumbStateF4RsMask          = 0x0038;
  static final protected int thumbStateF4RdMask          = 0x0007;
  static final protected int thumbStateF4AndBits         = 0x0000;
  static final protected int thumbStateF4EorBits         = 0x0040;
  static final protected int thumbStateF4LslBits         = 0x0080;
  static final protected int thumbStateF4LsrBits         = 0x00c0;
  static final protected int thumbStateF4AsrBits         = 0x0100;
  static final protected int thumbStateF4AdcBits         = 0x0140;
  static final protected int thumbStateF4SbcBits         = 0x0180;
  static final protected int thumbStateF4RorBits         = 0x01c0;
  static final protected int thumbStateF4TstBits         = 0x0200;
  static final protected int thumbStateF4NegBits         = 0x0240;
  static final protected int thumbStateF4CmpBits         = 0x0280;
  static final protected int thumbStateF4CmnBits         = 0x02c0;
  static final protected int thumbStateF4OrrBits         = 0x0300;
  static final protected int thumbStateF4MulBits         = 0x0340;
  static final protected int thumbStateF4BicBits         = 0x0380;
  static final protected int thumbStateF4MvnBits         = 0x03c0;

  static final protected int thumbStateF5InstructionMask = 0xfc00;
  static final protected int thumbStateF5InstructionBits = 0x4400;
  static final protected int thumbStateF5OpMask          = 0x0300;
  static final protected int thumbStateF5AddBits         = 0x0000;
  static final protected int thumbStateF5CmpBits         = 0x0100;
  static final protected int thumbStateF5MovBits         = 0x0200;
  static final protected int thumbStateF5BxBits          = 0x0300;
  static final protected int thumbStateF5H1Bit           = 0x0080;
  static final protected int thumbStateF5H2Bit           = 0x0040;
  static final protected int thumbStateF5RsMask          = 0x0038;
  static final protected int thumbStateF5RdMask          = 0x0007;

  static final protected int thumbStateF6InstructionMask = 0xf800;
  static final protected int thumbStateF6InstructionBits = 0x4800;
  static final protected int thumbStateF6RdMask          = 0x0700;
  static final protected int thumbStateF6ImmMask         = 0x00ff;

  static final protected int thumbStateF7InstructionMask = 0xf200;
  static final protected int thumbStateF7InstructionBits = 0x5000;
  static final protected int thumbStateF7LoadStoreBit    = 0x0800;
  static final protected int thumbStateF7ByteWordBit     = 0x0400;
  static final protected int thumbStateF7RoMask          = 0x01c0;
  static final protected int thumbStateF7RbMask          = 0x0038;
  static final protected int thumbStateF7RdMask          = 0x0007;

  static final protected int thumbStateF8InstructionMask = 0xf200;
  static final protected int thumbStateF8InstructionBits = 0x5200;
  static final protected int thumbStateF8HBit            = 0x0800;
  static final protected int thumbStateF8SignExtendedBit = 0x0400;
  static final protected int thumbStateF8RoMask          = 0x01c0;
  static final protected int thumbStateF8RbMask          = 0x0038;
  static final protected int thumbStateF8RdMask          = 0x0007;

  static final protected int thumbStateF9InstructionMask = 0xe000;
  static final protected int thumbStateF9InstructionBits = 0x6000;
  static final protected int thumbStateF9ByteWordBit     = 0x1000;
  static final protected int thumbStateF9LoadStoreBit    = 0x0800;
  static final protected int thumbStateF9OffsetMask      = 0x07c0;
  static final protected int thumbStateF9RbMask          = 0x0038;
  static final protected int thumbStateF9RdMask          = 0x0007;

  static final protected int thumbStateF10InstructionMask = 0xf000;
  static final protected int thumbStateF10InstructionBits = 0x8000;
  static final protected int thumbStateF10LoadStoreBit    = 0x0800;
  static final protected int thumbStateF10OffsetMask      = 0x07c0;
  static final protected int thumbStateF10RbMask          = 0x0038;
  static final protected int thumbStateF10RdMask          = 0x0007;

  static final protected int thumbStateF11InstructionMask = 0xf000;
  static final protected int thumbStateF11InstructionBits = 0x9000;
  static final protected int thumbStateF11LoadStoreBit    = 0x0800;
  static final protected int thumbStateF11RdMask          = 0x0700;
  static final protected int thumbStateF11ImmMask         = 0x00ff;

  static final protected int thumbStateF12InstructionMask = 0xf000;
  static final protected int thumbStateF12InstructionBits = 0xa000;
  static final protected int thumbStateF12SPBit           = 0x0800;
  static final protected int thumbStateF12RdMask          = 0x0700;
  static final protected int thumbStateF12OffsetMask      = 0x00ff;

  static final protected int thumbStateF13InstructionMask = 0xff00;
  static final protected int thumbStateF13InstructionBits = 0xb000;
  static final protected int thumbStateF13SignBit         = 0x0080;
  static final protected int thumbStateF13ImmediateMask   = 0x007f;

  static final protected int thumbStateF14InstructionMask = 0xf600;
  static final protected int thumbStateF14InstructionBits = 0xb400;
  static final protected int thumbStateF14LoadStoreBit    = 0x0800;
  static final protected int thumbStateF14PcLrBit         = 0x0100;

  static final protected int thumbStateF15InstructionMask = 0xf000;
  static final protected int thumbStateF15InstructionBits = 0xc000;
  static final protected int thumbStateF15LoadStoreBit    = 0x0800;
  static final protected int thumbStateF15RbMask          = 0x0700;

  static final protected int thumbStateF16InstructionMask = 0xf000;
  static final protected int thumbStateF16InstructionBits = 0xd000;
  static final protected int thumbStateF16CondMask        = 0x0f00;
  static final protected int thumbStateF16SOffsetMask     = 0x00ff;
  static final protected int thumbStateF16EQBits          = 0x0000;
  static final protected int thumbStateF16NEBits          = 0x0100;
  static final protected int thumbStateF16CSBits          = 0x0200;
  static final protected int thumbStateF16CCBits          = 0x0300;
  static final protected int thumbStateF16MIBits          = 0x0400;
  static final protected int thumbStateF16PLBits          = 0x0500;
  static final protected int thumbStateF16VSBits          = 0x0600;
  static final protected int thumbStateF16VCBits          = 0x0700;
  static final protected int thumbStateF16HIBits          = 0x0800;
  static final protected int thumbStateF16LSBits          = 0x0900;
  static final protected int thumbStateF16GEBits          = 0x0a00;
  static final protected int thumbStateF16LTBits          = 0x0b00;
  static final protected int thumbStateF16GTBits          = 0x0c00;
  static final protected int thumbStateF16LEBits          = 0x0d00;

  static final protected int thumbStateF17InstructionMask = 0xff00;
  static final protected int thumbStateF17InstructionBits = 0xdf00;

  static final protected int thumbStateF18InstructionMask = 0xf800;
  static final protected int thumbStateF18InstructionBits = 0xe000;
  static final protected int thumbStateF18OffsetMask      = 0x07ff;

  static final protected int thumbStateF19InstructionMask = 0xf000;
  static final protected int thumbStateF19InstructionBits = 0xf000;
  static final protected int thumbStateF19LowHiOffsetBit  = 0x0800;
  static final protected int thumbStateF19OffsetMask      = 0x07ff;
	
public Core(Memory memory) {

	// Assign memory interface
	this.memory = memory;

	// Initialise registers
	registers = new int[PSR_MODE + 1][18];
	currentRegisters = new int[18];

	// Initialise condition table
	initConditionTable();
	
	// Reset core
	reset();
}
private boolean checkCondition(int condition) {

	// Get PSR flags
	int flags		= (getRegister(REG_CPSR) & INSTR_MASK_COND) >>> INSTR_SHFT_COND;

	// Return condition check
	return conditionTable[condition | flags];
}
public int decodeARMInstruction(int instruction) {
		
	// Check condition
	if(!checkCondition((instruction & INSTR_MASK_COND) >>> (INSTR_SHFT_COND - 4)))
		return 1;

	// Branch
	else if ((instruction & INSTR_AIM_B) == INSTR_AIT_B)
		return instrArmB(instruction);

	// Branch and Exchange
	else if ((instruction & INSTR_AIM_BX) == INSTR_AIT_BX)
		return instrArmBX(instruction);

	// PSR Transfer, State to Registry (MSR)
	else if ((instruction & INSTR_AIM_MSR) == INSTR_AIT_MSR)
		return instrArmMSR(instruction);
	
	// PSR Transfer, Registry to State (MRS)
	else if ((instruction & INSTR_AIM_MRS) == INSTR_AIT_MRS)
		return instrArmMRS(instruction);
		
	// Multiply (and Accumulate)
	else if ((instruction & INSTR_AIM_MUL) == INSTR_AIT_MUL)
		return instrArmMUL(instruction);
	
	// Multiply Long (and Accumulate Long)
	else if ((instruction & INSTR_AIM_MULL) == INSTR_AIT_MULL)
		return instrArmMULL(instruction);
	
	// Single Data Transfer
	else if ((instruction & INSTR_AIM_LDRH) == INSTR_AIT_LDRH)
		return instrArmLDRH(instruction);
		
	// Single Data Swap (must be before LDR test)
	else if ((instruction & INSTR_AIM_SWP) == INSTR_AIT_SWP)
		return instrArmSWP(instruction);
			
	// Data Processing Test Operations
	else if ((instruction & INSTR_AIM_DATAPROCT) == INSTR_AIT_DATAPROCT) {

		// Fetch register Rn
		int regRnIndex = (instruction & INSTR_MASK_R_16_19) >> INSTR_SHFT_R_16_19;
		int regRdIndex = (instruction & INSTR_MASK_R_12_15) >> INSTR_SHFT_R_12_15;

		// Get operand 1 value
		int operand1 = getRegister(regRnIndex);
		int operand2 = 0;

		// Account for prefetch if operand1 is PC
		if(regRnIndex == REG_PC)
			operand1 += INSTR_SIZE_ARM;

		// Calculate second operand, immediate operand
		if ((instruction & INSTR_DP_IMMEDIATE) != 0) {

			// Get rotation value
			int rotate = (instruction & INSTR_DP_IMM_ROTATE) >> 7;

			// Get immediate value
			int value = (instruction & INSTR_DP_IMM_VALUE);

			// Calculate operand 2
			operand2 = (value >>> rotate) | (value << (32 - rotate));
		}

		// Calculate second operand, register operand
		else {

			// Fetch register Rm
			int regRmIndex = (instruction & INSTR_MASK_R_00_03) >> INSTR_SHFT_R_00_03;

			// Calculate operand 2
			operand2 = shift(regRmIndex, (instruction & INSTR_DP_REG_SHIFT) >> 4, true);
		}

		// Get op code
		int opCode = instruction & INSTR_DP_OP_MASK;

		// Execute Data Processing instructions
		switch (opCode) {

			// TST
			case INSTR_DP_OP_TST :
				return instrArmTST(operand1, operand2, regRdIndex);
				
			// TEQ
			case INSTR_DP_OP_TEQ :
				return instrArmTEQ(operand1, operand2, regRdIndex);
				
			// CMP
			case INSTR_DP_OP_CMP :
				return instrArmCMP(operand1, operand2, regRdIndex);
				
			// CMN
			case INSTR_DP_OP_CMN :
				return instrArmCMN(operand1, operand2, regRdIndex);				
		}
	}
		
	// Data Processing / PSR Transfer
	else if ((instruction & INSTR_AIM_DATAPROC) == INSTR_AIT_DATAPROC) {

		// Fetch register Rn
		int regRnIndex = (instruction & INSTR_MASK_R_16_19) >> INSTR_SHFT_R_16_19;
		int regRdIndex = (instruction & INSTR_MASK_R_12_15) >> INSTR_SHFT_R_12_15;

		// Get operand 1 value
		int operand1 = getRegister(regRnIndex);
		int operand2 = 0;

		// Account for prefetch if operand1 is PC
		if(regRnIndex == REG_PC)
			operand1 += INSTR_SIZE_ARM;
			
		// Calculate second operand, immediate operand
		if ((instruction & INSTR_DP_IMMEDIATE) != 0) {

			// Get rotation value
			int rotate = (instruction & INSTR_DP_IMM_ROTATE) >> 7;

			// Get immediate value
			int value = (instruction & INSTR_DP_IMM_VALUE);

			// Calculate operand 2
			operand2 = (value >>> rotate) | (value << (32 - rotate));
		}

		// Calculate second operand, register operand
		else {

			// Fetch register Rm
			int regRmIndex = (instruction & INSTR_MASK_R_00_03) >> INSTR_SHFT_R_00_03;

			// Calculate operand 2
			operand2 = shift(regRmIndex, (instruction & INSTR_DP_REG_SHIFT) >> 4, (instruction & INSTR_DP_SETFLAGS) != 0);
		}

		// Get op code
		int opCode = instruction & INSTR_DP_OP_MASK;

		// Execute Data Processing instructions
		switch (opCode) {

			// AND
			case INSTR_DP_OP_AND :
				return instrArmAND(operand1, operand2, regRdIndex, (instruction & INSTR_DP_SETFLAGS) != 0);

			// EOR
			case INSTR_DP_OP_EOR :
				return instrArmEOR(operand1, operand2, regRdIndex, (instruction & INSTR_DP_SETFLAGS) != 0);

			// SUB
			case INSTR_DP_OP_SUB :
				return instrArmSUB(operand1, operand2, regRdIndex, (instruction & INSTR_DP_SETFLAGS) != 0);
				
			// RSB
			case INSTR_DP_OP_RSB :
				return instrArmRSB(operand1, operand2, regRdIndex, (instruction & INSTR_DP_SETFLAGS) != 0);
				
			// ADD
			case INSTR_DP_OP_ADD :
				return instrArmADD(operand1, operand2, regRdIndex, (instruction & INSTR_DP_SETFLAGS) != 0);
				
			// ADC
			case INSTR_DP_OP_ADC :
				return instrArmADC(operand1, operand2, regRdIndex, (instruction & INSTR_DP_SETFLAGS) != 0);
				
			// SBC
			case INSTR_DP_OP_SBC :
				return instrArmSBC(operand1, operand2, regRdIndex, (instruction & INSTR_DP_SETFLAGS) != 0);
				
			// RSC
			case INSTR_DP_OP_RSC :
				return instrArmRSC(operand1, operand2, regRdIndex, (instruction & INSTR_DP_SETFLAGS) != 0);
				
			// ORR
			case INSTR_DP_OP_ORR :
				return instrArmORR(operand1, operand2, regRdIndex, (instruction & INSTR_DP_SETFLAGS) != 0);
				
			// MOV
			case INSTR_DP_OP_MOV :
				return instrArmMOV(operand1, operand2, regRdIndex, (instruction & INSTR_DP_SETFLAGS) != 0);
				
			// BIC
			case INSTR_DP_OP_BIC :
				return instrArmBIC(operand1, operand2, regRdIndex, (instruction & INSTR_DP_SETFLAGS) != 0);
				
			// MVN
			case INSTR_DP_OP_MVN :
				return instrArmMVN(operand1, operand2, regRdIndex, (instruction & INSTR_DP_SETFLAGS) != 0);
				
		}
	}
		
	// Undefined Instruction
	else if ((instruction & INSTR_AIM_UNDEF) == INSTR_AIT_UNDEF)
		return instrArmUNDEF(instruction);
		
	// Single Data Transfer
	else if ((instruction & INSTR_AIM_LDR) == INSTR_AIT_LDR)
		return instrArmLDR(instruction);
				
	// Block Data Transfer
	else if ((instruction & INSTR_AIM_LDM) == INSTR_AIT_LDM)
		return instrArmLDM(instruction);
		
	// Software Interrupt
	else if ((instruction & INSTR_AIM_SWI) == INSTR_AIT_SWI)
		return instrArmSWI(instruction);
	
		

	return -1;
}
public int executeARMInstruction() {
		
	// Fetch next WORD for execution pipe
	int instruction = memory.readWord(getRegister(REG_PC));

	// Advance PC
	setRegister(REG_PC, getRegister(REG_PC) + INSTR_SIZE_ARM);

	// Decode instruction
	return decodeARMInstruction(instruction);
}

public static long ctmLast=System.currentTimeMillis();

	public void executeNextInstruction() {
		Eladio.ou(getRegister(REG_PC));
		try {
			// Execute ARM instruction if in ARM state
			if ((getRegister(REG_CPSR) & PSR_FLAG_STATE) == 0)
				cycleCount += executeARMInstruction();

			// Execute THUMB instruction if in THUMB state
			else
				cycleCount += executeTHUMBInstruction();
		} catch (Exception e) {
			System.out.println("Error en " + Long.toHexString(getRegister(REG_PC)));
		}
	}

public int executeTHUMBInstruction() {

	// Fetch next WORD for execution pipe
	short instruction = (short)(0xffff & memory.readHalfWord(getRegister(REG_PC)));

	// Advance PC
	setRegister(REG_PC, getRegister(REG_PC) + INSTR_SIZE_THUMB);
	
	// Move shifted register (LSL)
	if((instruction & INSTR_TIM_LSL) == INSTR_TIT_LSL)
		return instrThumbLSL(instruction);
	
	// Move shifted register (LSR)
	if((instruction & INSTR_TIM_LSR) == INSTR_TIT_LSR)
		return instrThumbLSR(instruction);
	
	// Move shifted register (ASR)
	if((instruction & INSTR_TIM_ASR) == INSTR_TIT_ASR)
		return instrThumbASR(instruction);

	// Addition/substraction
	if((instruction & INSTR_TIM_ADD) == INSTR_TIT_ADD)
		return instrThumbADD(instruction);

	// Move/compare/add/substract immediate
	if((instruction & INSTR_TIM_MOV) == INSTR_TIT_MOV)
		return instrThumbMOV(instruction);

	// ALU operation
	if((instruction & INSTR_TIM_ALU) == INSTR_TIT_ALU)
		return instrThumbALU(instruction);

	// HIREG operation
	if((instruction & INSTR_TIM_HIREG) == INSTR_TIT_HIREG)
		return instrThumbHIREG(instruction);

	// PCLOAD operation
	if((instruction & INSTR_TIM_PCLOAD) == INSTR_TIT_PCLOAD)
		return instrThumbPCLOAD(instruction);

	// SPLOAD operation
	if((instruction & INSTR_TIM_SPLOAD) == INSTR_TIT_SPLOAD)
		return instrThumbSPLOAD(instruction);

	// LDR operation
	if((instruction & INSTR_TIM_LDR) == INSTR_TIT_LDR)
		return instrThumbLDR(instruction);

	// LDRHS operation
	if((instruction & INSTR_TIM_LDRHS) == INSTR_TIT_LDRHS)
		return instrThumbLDRHS(instruction);

	// LDRI operation
	if((instruction & INSTR_TIM_LDRI) == INSTR_TIT_LDRI)
		return instrThumbLDRI(instruction);

	// LDRH operation
	if((instruction & INSTR_TIM_LDRH) == INSTR_TIT_LDRH)
		return instrThumbLDRH(instruction);

	// LOADADDR operation
	if((instruction & INSTR_TIM_LOADADDR) == INSTR_TIT_LOADADDR)
		return instrThumbLOADADDR(instruction);

	// SPADD operation
	if((instruction & INSTR_TIM_SPADD) == INSTR_TIT_SPADD)
		return instrThumbSPADD(instruction);

	// PUSHPOP operation
	if((instruction & INSTR_TIM_PUSHPOP) == INSTR_TIT_PUSHPOP)
		return instrThumbPUSHPOP(instruction);

	// MULTLS operation
	if((instruction & INSTR_TIM_MULTLS) == INSTR_TIT_MULTLS)
		return instrThumbMULTLS(instruction);

	// SWI operation
	if((instruction & INSTR_TIM_SWI) == INSTR_TIT_SWI)
		return instrThumbSWI(instruction);
		
	// CBRANCH operation
	if((instruction & INSTR_TIM_CBRANCH) == INSTR_TIT_CBRANCH)
		return instrThumbCBRANCH(instruction);

	// BRANCH operation
	if((instruction & INSTR_TIM_BRANCH) == INSTR_TIT_BRANCH)
		return instrThumbBRANCH(instruction);

	// LBRANCH operation
	if((instruction & INSTR_TIM_LBRANCH) == INSTR_TIT_LBRANCH)
		return instrThumbLBRANCH(instruction);

	
	// Code shouldnt reach this point
	return 0;
}
public int getRegister(int registerIndex) {

	// Return register value
	return currentRegisters[registerIndex];
}
public boolean getRegisterBit(int registerIndex, int bit) {

	// Return bit state
	return (getRegister(registerIndex) & bit) != 0;
}
private void initConditionTable() {

	// Initialise memory
	conditionTable = new boolean[256];

	// Fill table
	for (int condition = 0; condition < 16; condition++)
		for (int flags = 0; flags < 16; flags++) {

			// By default, condition check succeeds (instruction can be executed
			boolean result = true;

			// Check conditions
			switch(condition) {

				case 0x0	:	result = (flags & 0x4) != 0; break; // EQ
				case 0x1	:	result = (flags & 0x4) == 0; break; // NE
				case 0x2	:	result = (flags & 0x2) != 0; break; // CS
				case 0x3	:	result = (flags & 0x2) == 0; break; // CC
				case 0x4	:	result = (flags & 0x8) != 0; break; // MI
				case 0x5	:	result = (flags & 0x8) == 0; break; // PL
				case 0x6	:	result = (flags & 0x1) != 0; break; // VS
				case 0x7	:	result = (flags & 0x1) == 0; break; // VC
				case 0x8	:	result = ((flags & 0x2) != 0) && ((flags & 0x4) == 0); break; // HI
				case 0x9	:	result = ((flags & 0x2) == 0) || ((flags & 0x4) != 0); break; // LS
				case 0xa	:	result = ((flags & 0x8) != 0) == ((flags & 0x1) != 0); break; // GE
				case 0xb	:	result = ((flags & 0x8) != 0) != ((flags & 0x1) != 0); break; // LT				
				case 0xc	:	result = ((flags & 0x4) == 0) && ((flags & 0x8) != 0) == ((flags & 0x1) != 0); break; // GT
				case 0xd	:	result = ((flags & 0x4) != 0) || ((flags & 0x8) != 0) != ((flags & 0x1) != 0); break; // LE
			}
			
			// Store result
			conditionTable[(condition << 4) | flags] = result;
		}
}
private final int instrArmADC(int operand1, int operand2, int registerIndex, boolean setFlags) {

	// Calculate result
	int result = operand1 + operand2 + (getRegisterBit(REG_CPSR, PSR_FLAG_CARRY) ? 1 : 0);

	// Set PSR flags
	if(setFlags) {
		
		setRegisterBit(REG_CPSR, PSR_FLAG_ZERO, result == 0);
		setRegisterBit(REG_CPSR, PSR_FLAG_NEGATIVE, result < 0);
		setAddCarryOverflowFlags(operand1, operand2, result);

		// If register is PC copy SPSR of current mode to CPSR
		if(registerIndex == REG_PC)
			setRegister(REG_CPSR, getRegister(REG_SPSR));
	}
		
	// Write result to register
	setRegister(registerIndex, result);

	// Return number of cycles
	return 3;
}
private final int instrArmADD(int operand1, int operand2, int registerIndex, boolean setFlags) {

	// Calculate result
	int result = operand1 + operand2;
	
	// Set PSR flags
	if(setFlags) {
		
		setRegisterBit(REG_CPSR, PSR_FLAG_ZERO, result == 0);
		setRegisterBit(REG_CPSR, PSR_FLAG_NEGATIVE, result < 0);
		setAddCarryOverflowFlags(operand1, operand2, result);

		// If register is PC copy SPSR of current mode to CPSR
		if(registerIndex == REG_PC)
			setRegister(REG_CPSR, getRegister(REG_SPSR));
	}
	
	// Write result to register
	setRegister(registerIndex, result);

	// Return number of cycles
	return 3;
}
private final int instrArmAND(int operand1, int operand2, int registerIndex, boolean setFlags) {

	// Calculate result
	int result = operand1 & operand2;

	// Set PSR flags
	if(setFlags) {
		
		setRegisterBit(REG_CPSR, PSR_FLAG_ZERO, result == 0);
		setRegisterBit(REG_CPSR, PSR_FLAG_NEGATIVE, result < 0);

		// If register is PC copy SPSR of current mode to CPSR
		if(registerIndex == REG_PC)
			setRegister(REG_CPSR, getRegister(REG_SPSR));
	}
		
	// Write result to register
	setRegister(registerIndex, result);

	// Return number of cycles
	return 3;
}
private final int instrArmB(int instruction) {

	// Fetch register Rn (and sign extend)
	int offset = (((instruction & INSTR_B_OFFSET) << 8) >> 6) + INSTR_SIZE_ARM;	
	
	// Store current PC in R14 if BL
	if((instruction & INSTR_B_LINKBIT) != 0)
		setRegister(REG_R14, getRegister(REG_PC) & 0xfffffffc);

	// Add offset to PC
	setRegister(REG_PC, getRegister(REG_PC) + offset);
	
	// Return number of cycles
	return 3;	
}
private final int instrArmBIC(int operand1, int operand2, int registerIndex, boolean setFlags) {

	// Calculate result
	int result = operand1 & ~operand2;

	// Set PSR flags
	if(setFlags) {
		
		setRegisterBit(REG_CPSR, PSR_FLAG_ZERO, result == 0);
		setRegisterBit(REG_CPSR, PSR_FLAG_NEGATIVE, result < 0);

		// If register is PC copy SPSR of current mode to CPSR
		if(registerIndex == REG_PC)
			setRegister(REG_CPSR, getRegister(REG_SPSR));
	}
	
	// Write result to register
	setRegister(registerIndex, result);

	// Return number of cycles
	return 3;
}
private final int instrArmBX(int instruction) {

	// Fetch register Rn
	int registerIndex = instruction & INSTR_MASK_R_00_03;

	// Get register value
	int registerValue = getRegister(registerIndex);

	// Store value in PC
	setRegister(REG_PC, registerValue & 0xfffffffe);

	// Store state bit
	setRegisterBit(REG_CPSR, PSR_FLAG_STATE, (registerValue & 0x00000001) != 0);

	// Return number of cycles
	return 3;
}
private final int instrArmCMN(int operand1, int operand2, int registerIndex) {

	// Calculate result
	int result = operand1 + operand2;

	// Set PSR flags
	setRegisterBit(REG_CPSR, PSR_FLAG_ZERO, result == 0);
	setRegisterBit(REG_CPSR, PSR_FLAG_NEGATIVE, result < 0);
	setAddCarryOverflowFlags(operand1, operand2, result);
		
	// Return number of cycles
	return 3;
}
private final int instrArmCMP(int operand1, int operand2, int registerIndex) {

	// Calculate result
	int result = operand1 - operand2;

	// Set PSR flags
	setRegisterBit(REG_CPSR, PSR_FLAG_ZERO, result == 0);
	setRegisterBit(REG_CPSR, PSR_FLAG_NEGATIVE, result < 0);
	setSubCarryOverflowFlags(operand1, operand2, result);
		
	// Return number of cycles
	return 3;
}
private final int instrArmEOR(int operand1, int operand2, int registerIndex, boolean setFlags) {

	// Calculate result
	int result = operand1 ^ operand2;

	// Set PSR flags
	if(setFlags) {
		
		setRegisterBit(REG_CPSR, PSR_FLAG_ZERO, result == 0);
		setRegisterBit(REG_CPSR, PSR_FLAG_NEGATIVE, result < 0);

		// If register is PC copy SPSR of current mode to CPSR
		if(registerIndex == REG_PC)
			setRegister(REG_CPSR, getRegister(REG_SPSR));
	}	
		
	// Write result to register
	setRegister(registerIndex, result);

	// Return number of cycles
	return 3;
}
private final int instrArmLDM(int instruction) {

	// Get register indices
	int regRnIndex		= (instruction & INSTR_MASK_R_16_19) >> INSTR_SHFT_R_16_19;
	
	// Get flags
	boolean preIndexing	= (instruction & INSTR_LDM_INDEXING) != 0;
	boolean upOffset	= (instruction & INSTR_LDM_UP) != 0;
	boolean psrForce	= (instruction & INSTR_LDM_PSRFORCE) != 0;
	boolean writeBack	= (instruction & INSTR_LDM_WRITEBACK) != 0;
	boolean load		= (instruction & INSTR_LDM_LOAD) != 0;

	if(regRnIndex == REG_PC)
		System.out.println("LDM : regRnIndex is PC!");
	
	// Get base offset
	int baseOffset		= getRegister(regRnIndex) & 0xfffffffc;

	// Adjust for prefetch (R15 shouldnt be base though)
	if(regRnIndex == REG_PC)
		baseOffset += INSTR_SIZE_ARM;

	// Initialise active bit value
	int activeRegister	= upOffset ? 1 : (1 << 15);

	// Write/load registers
	for (int i = 0; i < 16; i++){

		// Check if register should be loaded/stored
		if((instruction & activeRegister) != 0) {

			// If pre-indexing is active adjust offset first
			if(preIndexing)
				baseOffset += upOffset ? 4 : -4;

			// Load/write data
			if(load) {
				setRegister(upOffset ? i : (15 - i), memory.readWord(baseOffset));

				if(psrForce & (upOffset ? i : (15 - i)) == REG_PC)
					setRegister(REG_CPSR, getRegister(REG_SPSR));
			}
			else {

				// Get value
				int value = psrForce ? registers[PSR_MODE_USER][upOffset ? i : (15 - i)] : getRegister(upOffset ? i : (15 - i));

				//Adjust for prefetch
				if((upOffset ? i : (15 - i)) == REG_PC)
					value += INSTR_SIZE_ARM << 1; // Prefetch is 12 ahead
				
				// Write to memory
				memory.writeWord(value, baseOffset);
			}
				
			// If post-indexing is active adjust offset now
			if(!preIndexing)
				baseOffset += upOffset ? 4 : -4;
		}

		// Set next register bit
		if(upOffset)
			activeRegister <<= 1;
		else
			activeRegister >>>= 1;
	}

	// Do write back if needed
	if(writeBack)
		setRegister(regRnIndex, baseOffset);
	
	// Print error for unimplemented feature
	if(psrForce)
		System.out.println("ERROR: Unimplemented feature in LDM/STM instruction");
	
	// Return number of cycles
	return 3;	
}
private final int instrArmLDR(int instruction) {

	// Get register indices
	int regRnIndex		= (instruction & INSTR_MASK_R_16_19) >> INSTR_SHFT_R_16_19;
	int regRdIndex		= (instruction & INSTR_MASK_R_12_15) >> INSTR_SHFT_R_12_15;

	// Get flags
	boolean immediate	= (instruction & INSTR_LDR_IMMEDIATE) == 0;
	boolean preIndexing	= (instruction & INSTR_LDR_INDEXING) != 0;
	boolean upOffset	= (instruction & INSTR_LDR_UP) != 0;
	boolean byteOp		= (instruction & INSTR_LDR_BYTE) != 0;
	boolean writeBack	= (instruction & INSTR_LDR_WRITEBACK) != 0 | !preIndexing;
	boolean load		= (instruction & INSTR_LDR_LOAD) != 0;

	// Get base offset
	int baseOffset		= getRegister(regRnIndex);

	// Adjust for prefetch if base register is PC
	if(regRnIndex == REG_PC)
		baseOffset += INSTR_SIZE_ARM;
	
	// Get offset
	int offset			= (instruction & INSTR_LDR_OFFSET);
		
	// Adjust offset if offset is register defined
	if(!immediate) {

		// Get shift register
		int registerIndex = (instruction & INSTR_LDR_SHIFTREG);

		// Calculate offset
		offset = shift(registerIndex, offset >> 4, false);
	}

	// If pre-indexing is active, add offset first
	if(preIndexing)
		baseOffset += upOffset ? offset : -offset;


		
	// Handle load operations
	if(load) {

		if(byteOp)
			setRegister(regRdIndex, 0xff & memory.readByte(baseOffset));
		else {
			setRegister(regRdIndex, memory.readWord(baseOffset));
			
			if((baseOffset & 3) != 0)
				System.out.println("NON ALIGNED LDM!");
		}
//		else
//			setRegister(regRdIndex, (memory.readHalfWord(baseOffset + 2) << 16) | memory.readHalfWord(baseOffset));
	}

	// Handle write operations
	else {

		// Read register value
		int value = getRegister(regRdIndex);

		// If register is PC advance value
		if(regRdIndex == REG_PC)
			value += INSTR_SIZE_ARM << 1; // Prefetch is 12 ahead

		// Write to memory
		if(byteOp)
			memory.writeByte((byte)(0xff & value), baseOffset);
		else
			memory.writeWord(value, baseOffset & 0xfffffffc);
	}
	
		
	// If post-indexing is active, add offset now
	if(!preIndexing)
		baseOffset += upOffset ? offset : -offset;

	// Write back base offset if needed
	if(writeBack || !preIndexing)
		setRegister(regRnIndex, baseOffset);
		
	// Return number of cycles
	return 3;	
}
private final int instrArmLDRH(int instruction) {

	// Define locals
	int offset, value;
	
	// Get register indices
	int regRnIndex		= (instruction & INSTR_MASK_R_16_19) >> INSTR_SHFT_R_16_19;
	int regRdIndex		= (instruction & INSTR_MASK_R_12_15) >> INSTR_SHFT_R_12_15;

	// Get flags
	boolean immediate	= (instruction & INSTR_LDRH_IMMEDIATE) != 0;
	boolean preIndexing	= (instruction & INSTR_LDR_INDEXING) != 0;
	boolean upOffset	= (instruction & INSTR_LDR_UP) != 0;
	boolean byteOp		= (instruction & INSTR_LDR_BYTE) != 0;
	boolean writeBack	= (instruction & INSTR_LDR_WRITEBACK) != 0 | !preIndexing;
	boolean load		= (instruction & INSTR_LDR_LOAD) != 0;
	boolean signed		= (instruction & INSTR_LDRH_SIGNED) != 0;
	boolean halfword	= (instruction & INSTR_LDRH_HALFWORD) != 0;

	// Get base offset
	int baseOffset		= getRegister(regRnIndex);
	
	// Adjust for prefetch if base register is PC
	if(regRnIndex == REG_PC)
		baseOffset += INSTR_SIZE_ARM;
	
	// Get immediate offset
	if(immediate) {

		// Get offset
		offset = ((instruction & INSTR_LDRH_OFFSET_H) >> 4) | (instruction & INSTR_LDRH_OFFSET_L);
	}

	// Get register defined offset
	else {

		// Get register index
		int regRmIndex	= (instruction & INSTR_MASK_R_00_03);

		// Get offset
		offset 			= getRegister(regRmIndex);

		// Adjut for prefetch
		if(regRmIndex == REG_PC)
			offset += INSTR_SIZE_ARM;
	}
			
	// If pre-indexing is active, add offset first
	if(preIndexing)
		baseOffset += upOffset ? offset : -offset;


		
	// Handle load operations
	if(load) {

		// Handle halfword loads
		if(halfword) {
			
			// Read halfword
			value = (0xffff & memory.readHalfWord(baseOffset));

			// Sign extend if needed
			if(signed & (value & 0x00008000) != 0)
				value |= 0xffff0000;
		}

		// Handle byte loads
		else {
			
			// Read byte
			value = (0xff & memory.readByte(baseOffset));

			// Sign extend if needed
			if(signed & (value & 0x00000080) != 0)
				value |= 0xfffffff0;
		}

		// Store value in register
		setRegister(regRdIndex, value);
	}

	// Handle write operations
	else {

		// Read register value
		value = getRegister(regRdIndex);

		// Adjust for prefetch
		if(regRdIndex == REG_PC)
			value += INSTR_SIZE_ARM << 1; // Prefetch is 12 ahead

		// Write to memory
		if(halfword)
			memory.writeHalfWord((short)(0xffff & value), baseOffset);
		else
			memory.writeByte((byte)(0xff & value), baseOffset);
	}
	
		
	// If post-indexing is active, add offset now
	if(!preIndexing)
		baseOffset += upOffset ? offset : -offset;

	// Write back base offset if needed
	if(writeBack || !preIndexing)
		setRegister(regRnIndex, baseOffset);
		
	// Return number of cycles
	return 3;	
}
private final int instrArmMOV(int operand1, int operand2, int registerIndex, boolean setFlags) {

	// Get result
	int result = operand2;
	
	// Write result to register
	setRegister(registerIndex, result);

	// Set PSR flags
	if(setFlags) {
		
		setRegisterBit(REG_CPSR, PSR_FLAG_ZERO, result == 0);
		setRegisterBit(REG_CPSR, PSR_FLAG_NEGATIVE, result < 0);

		// If register is PC copy SPSR of current mode to CPSR
		if(registerIndex == REG_PC)
			setRegister(REG_CPSR, getRegister(REG_SPSR));
	}

	// Return number of cycles
	return 3;
}
private final int instrArmMRS(int instruction) {

	// Get destination register index
	int registerIndex = (instruction & INSTR_MASK_R_12_15) >> INSTR_SHFT_R_12_15;
	
	// Write CPSR or SPSR to register
	setRegister(registerIndex, (instruction & INSTR_MRS_SPSR_FLAG) == 0 ? getRegister(REG_CPSR) : getRegister(REG_SPSR));
		
	// Return number of cycles
	return 1;
}
private final int instrArmMSR(int instruction) {

	// MSR Type : Register to PSR flags only
	if((instruction & INSTR_MSR_TYPE_FLAG) == 0) {

		// Get current CPSR/SPSR value and remove flags
		int currentPSR = getRegister((instruction & INSTR_MRS_SPSR_FLAG) == 0 ? REG_CPSR : REG_SPSR) & 0x0fffffff;
		
		// Source operand is register
		if((instruction & INSTR_MSR_SRC_FLAG) == 0) {
			
			// Get register index
			int registerIndex = instruction & INSTR_MASK_R_00_03;

			// Write contents to CPSR or SPSR flags
			setRegister((instruction & INSTR_MRS_SPSR_FLAG) == 0 ? REG_CPSR : REG_SPSR, currentPSR | (getRegister(registerIndex) & 0xf0000000));
		}

		// Source operand is immediate value
		else {

			// Get rotation value
			int rotate = (instruction & INSTR_MSR_IMM_ROTATE) >> 7;

			// Get immediate value
			int value = (instruction & INSTR_MSR_IMM_VALUE);

			// Calculate operand 2
			value = (value >>> rotate) | (value << (32 - rotate));

			// Write contents to CPSR or SPSR flags
			setRegister((instruction & INSTR_MRS_SPSR_FLAG) == 0 ? REG_CPSR : REG_SPSR, currentPSR | (value & 0xf0000000));
		}		
	}
	
	// MSR Type : Register to PSR
	else {

		// Get register indices
		int regRsIndex = instruction & INSTR_MASK_R_00_03;
		int regRdIndex = (instruction & INSTR_MRS_SPSR_FLAG) == 0 ? REG_CPSR : REG_SPSR;

		// Get register value
		int value = getRegister(regRsIndex);

		// Adjust for prefetch
		if(regRsIndex == REG_PC)
			value += INSTR_SIZE_ARM;

		// Write contents to CPSR or SPSR (only allow non user mode writes)
		if((getRegister(REG_CPSR) & PSR_MODE) != PSR_MODE_USER)
			setRegister(regRdIndex, value);
		else
			setRegister(regRdIndex, ((getRegister(regRdIndex) + (regRdIndex == REG_PC ? INSTR_SIZE_ARM : 0)) & 0x0fffffff) | (value & 0xf0000000));
	}
	
	// Return number of cycles
	return 1;
}
private final int instrArmMUL(int instruction) {

	// Get register indices
	int regRdIndex = (instruction & INSTR_MASK_R_16_19) >>> INSTR_SHFT_R_16_19;
	int regRsIndex = (instruction & INSTR_MASK_R_08_11) >>> INSTR_SHFT_R_08_11;
	int regRmIndex = (instruction & INSTR_MASK_R_00_03) >>> INSTR_SHFT_R_00_03;		
	
	// Calculate result
	int result = (getRegister(regRmIndex) + (regRmIndex == REG_PC ? INSTR_SIZE_ARM : 0)) * (getRegister(regRsIndex) + (regRsIndex == REG_PC ? INSTR_SIZE_ARM : 0));

	// Accumulate if needed
	if((instruction & INSTR_MUL_ACC_FLAG) != 0) {

		// Get register index
		int regRnIndex = (instruction & INSTR_MASK_R_12_15) >> INSTR_SHFT_R_12_15;

		// Add register value to result
		result += (getRegister(regRnIndex) + (regRnIndex == REG_PC ? INSTR_SIZE_ARM : 0));
	}

	// Set condition flags if needed
	if((instruction & INSTR_MUL_COND_FLAG) != 0) {

		// Set PSR flags
		setRegisterBit(REG_CPSR, PSR_FLAG_ZERO, result == 0);
		setRegisterBit(REG_CPSR, PSR_FLAG_NEGATIVE, result < 0);		
	}

	// Store result in register
	setRegister(regRdIndex, result);

	// Return number of cycles
	return 3;
}
private final int instrArmMULL(int instruction) {

	// Get register indices
	int regRmIndex		= (instruction & INSTR_MASK_R_00_03);
	int regRsIndex		= (instruction & INSTR_MASK_R_08_11) >> INSTR_SHFT_R_08_11;
	int regRdLoIndex	= (instruction & INSTR_MASK_R_12_15) >> INSTR_SHFT_R_12_15;
	int regRdHiIndex	= (instruction & INSTR_MASK_R_16_19) >> INSTR_SHFT_R_16_19;

	// Initialise result
	long result = 0;

	// Handle signed multiplication
	if((instruction & INSTR_MULL_UNSIGNED) != 0) {

		// Calculate result
		result = (long)getRegister(regRmIndex) * (long)getRegister(regRsIndex);

		// Accumulate if needed
		if((instruction & INSTR_MULL_ACCUM) != 0)
		  result += ((long)getRegister(regRdHiIndex) << 32) | (0xffffffffl & (long)getRegister(regRdLoIndex));
	}

	// Handle unsigned multiplication (FIXME : not correct)
	else {

		// Calculate result
		result = (long)getRegister(regRmIndex) * (long)getRegister(regRsIndex);

		// Accumulate if needed
		if((instruction & INSTR_MULL_ACCUM) != 0)
		  result += ((long)getRegister(regRdHiIndex) << 32) | (0xffffffffl & (long)getRegister(regRdLoIndex));
	}
		
	// Set condition flags if needed
	if((instruction & INSTR_MUL_COND_FLAG) != 0) {

		// Set PSR flags
		setRegisterBit(REG_CPSR, PSR_FLAG_ZERO, result == 0);
		setRegisterBit(REG_CPSR, PSR_FLAG_NEGATIVE, result < 0);		
	}

	// Return number of cycles
	return 3;
}
private final int instrArmMVN(int operand1, int operand2, int registerIndex, boolean setFlags) {

	// Get result
	int result = ~operand2;
	
	// Write result to register
	setRegister(registerIndex, result);

	// Set PSR flags
	if(setFlags) {
		
		setRegisterBit(REG_CPSR, PSR_FLAG_ZERO, result == 0);
		setRegisterBit(REG_CPSR, PSR_FLAG_NEGATIVE, result < 0);

		// If register is PC copy SPSR of current mode to CPSR
		if(registerIndex == REG_PC)
			setRegister(REG_CPSR, getRegister(REG_SPSR));
	}
		
	// Return number of cycles
	return 3;
}
private final int instrArmORR(int operand1, int operand2, int registerIndex, boolean setFlags) {

	// Calculate result
	int result = operand1 | operand2;

	// Set PSR flags
	if(setFlags) {
		
		setRegisterBit(REG_CPSR, PSR_FLAG_ZERO, result == 0);
		setRegisterBit(REG_CPSR, PSR_FLAG_NEGATIVE, result < 0);

		// If register is PC copy SPSR of current mode to CPSR
		if(registerIndex == REG_PC)
			setRegister(REG_CPSR, getRegister(REG_SPSR));
	}
		
	// Write result to register
	setRegister(registerIndex, result);

	// Return number of cycles
	return 3;
}
private final int instrArmRSB(int operand1, int operand2, int registerIndex, boolean setFlags) {

	// Calculate result
	int result = operand2 - operand1;

	// Set PSR flags
	if(setFlags) {
		
		setRegisterBit(REG_CPSR, PSR_FLAG_ZERO, result == 0);
		setRegisterBit(REG_CPSR, PSR_FLAG_NEGATIVE, result < 0);
		setSubCarryOverflowFlags(operand2, operand1, result);

		// If register is PC copy SPSR of current mode to CPSR
		if(registerIndex == REG_PC)
			setRegister(REG_CPSR, getRegister(REG_SPSR));
	}
		
	// Write result to register
	setRegister(registerIndex, result);

	// Return number of cycles
	return 3;
}
private final int instrArmRSC(int operand1, int operand2, int registerIndex, boolean setFlags) {

	// Calculate result
	int result = operand2 - operand1 + (getRegisterBit(REG_CPSR, PSR_FLAG_CARRY) ? 1 : 0) - 1;

	// Set PSR flags
	if(setFlags) {
		
		setRegisterBit(REG_CPSR, PSR_FLAG_ZERO, result == 0);
		setRegisterBit(REG_CPSR, PSR_FLAG_NEGATIVE, result < 0);
		setSubCarryOverflowFlags(operand2, operand1, result);

		// If register is PC copy SPSR of current mode to CPSR
		if(registerIndex == REG_PC)
			setRegister(REG_CPSR, getRegister(REG_SPSR));
	}
		
	// Write result to register
	setRegister(registerIndex, result);

	// Return number of cycles
	return 3;
}
private final int instrArmSBC(int operand1, int operand2, int registerIndex, boolean setFlags) {

	// Calculate result
	int result = operand1 - operand2 + (getRegisterBit(REG_CPSR, PSR_FLAG_CARRY) ? 1 : 0) - 1;

	// Set PSR flags
	if(setFlags) {
		
		setRegisterBit(REG_CPSR, PSR_FLAG_ZERO, result == 0);
		setRegisterBit(REG_CPSR, PSR_FLAG_NEGATIVE, result < 0);
		setSubCarryOverflowFlags(operand1, operand2, result);

		// If register is PC copy SPSR of current mode to CPSR
		if(registerIndex == REG_PC)
			setRegister(REG_CPSR, getRegister(REG_SPSR));
	}
		
	// Write result to register
	setRegister(registerIndex, result);

	// Return number of cycles
	return 3;
}
private final int instrArmSUB(int operand1, int operand2, int registerIndex, boolean setFlags) {

	// Calculate result
	int result = operand1 - operand2;

	// Set PSR flags
	if(setFlags) {
			
		setRegisterBit(REG_CPSR, PSR_FLAG_ZERO, result == 0);
		setRegisterBit(REG_CPSR, PSR_FLAG_NEGATIVE, result < 0);
		setSubCarryOverflowFlags(operand1, operand2, result);

		// If register is PC copy SPSR of current mode to CPSR
		if(registerIndex == REG_PC)
			setRegister(REG_CPSR, getRegister(REG_SPSR));
	}
		
	// Write result to register
	setRegister(registerIndex, result);

	// Return number of cycles
	return 3;
}
private final int instrArmSWI(int instruction) {

	// Get current CPSR value
	int currentPSR = getRegister(REG_CPSR);

	// Save current PC is LR_svc
	registers[PSR_MODE_SUPERVISOR][REG_SPSR] = getRegister(REG_CPSR);
	registers[PSR_MODE_SUPERVISOR][REG_LR] = getRegister(REG_PC);
	
	// Move to supervisor mode and force ARM mode
	setRegister(REG_CPSR, ((currentPSR & 0xffffffc0) & ~PSR_FLAG_STATE) | PSR_MODE_SUPERVISOR);
	
	// Force PC to exception table entry point
	setRegister(REG_PC, 0x00000008);
	
	// Return number of cycles
	return 4;	
}
private final int instrArmSWP(int instruction) {

	// Get register indices
	int regRnIndex		= (instruction & INSTR_MASK_R_16_19) >> INSTR_SHFT_R_16_19;
	int regRdIndex		= (instruction & INSTR_MASK_R_12_15) >> INSTR_SHFT_R_12_15;
	int regRmIndex		= (instruction & INSTR_MASK_R_00_03);

	// Get flags
	boolean byteOp		= (instruction & INSTR_SWP_BYTE) != 0;

	if(regRnIndex == REG_PC)
		System.out.println("SWP : regRnIndex is PC!");
	// Get base offset
	int baseOffset		= getRegister(regRnIndex);

	// Adjust for prefetch
	if(regRnIndex == REG_PC)
		baseOffset += INSTR_SIZE_ARM;
	
	// Read value from swap address
	int value			= (byteOp ? (0xff & memory.readByte(baseOffset)) : memory.readWord(baseOffset));

	// Write contents of register to swap address
	if(byteOp)
		memory.writeByte((byte)(0xff & getRegister(regRmIndex)), baseOffset);
	else
		memory.writeWord(getRegister(regRmIndex), baseOffset);

	// Store old value in destination register
	setRegister(regRdIndex, value);
		
	// Return number of cycles
	return 3;	
}
private final int instrArmTEQ(int operand1, int operand2, int registerIndex) {

	// Calculate result
	int result = operand1 ^ operand2;

	// Set PSR flags
	setRegisterBit(REG_CPSR, PSR_FLAG_ZERO, result == 0);
	setRegisterBit(REG_CPSR, PSR_FLAG_NEGATIVE, result < 0);
		
	// Return number of cycles
	return 3;
}
private final int instrArmTST(int operand1, int operand2, int registerIndex) {

	// Calculate result
	int result = operand1 & operand2;

	// Set PSR flags
	setRegisterBit(REG_CPSR, PSR_FLAG_ZERO, result == 0);
	setRegisterBit(REG_CPSR, PSR_FLAG_NEGATIVE, result < 0);

	// Return number of cycles
	return 3;
}
private final int instrArmUNDEF(int instruction) {

	// Return number of cycles
	return 4;	
}
private final int instrThumbADD(short instruction) {	
	
	// Get source and destination registers
	int regRsIndex		= (instruction & INSTR_MASK_R_03_05) >> INSTR_SHFT_R_03_05;
	int regRdIndex		= (instruction & INSTR_MASK_R_00_02) >> INSTR_SHFT_R_00_02;
	
	// Get operands
	int operand1 		= getRegister(regRsIndex);
	int operand2		= (instruction & INSTR_ADD_IMMEDIATE) != 0 ? (instruction & INSTR_ADD_OFFSET) >>> 6 : getRegister((instruction & INSTR_ADD_OFFSET) >>> 6);
	int result;

	// Calculate result
	if((instruction & INSTR_ADD_OP) != 0) {

		// Calculate result
		result = operand1 - operand2;
		
		// Set PSR flags
		setRegisterBit(REG_CPSR, PSR_FLAG_ZERO, result == 0);
		setRegisterBit(REG_CPSR, PSR_FLAG_NEGATIVE, result < 0);
		
		// Set flags
		setSubCarryOverflowFlags(operand1, operand2, result);		
	}
	else {

		// Calculate result
		result = operand1 + operand2;
		
		// Set PSR flags
		setRegisterBit(REG_CPSR, PSR_FLAG_ZERO, result == 0);
		setRegisterBit(REG_CPSR, PSR_FLAG_NEGATIVE, result < 0);
		
		// Set flags
		setAddCarryOverflowFlags(operand1, operand2, result);
	}
	
	// Calculate result
	setRegister(regRdIndex, result);
	
	// Return number of cycles
	return 3;
}
private final int instrThumbALU(short instruction) {

	// Get source and destination registers
	int regRsIndex		= (instruction & INSTR_MASK_R_03_05) >> INSTR_SHFT_R_03_05;
	int regRdIndex		= (instruction & INSTR_MASK_R_00_02) >> INSTR_SHFT_R_00_02;

	// Get operands
	int operand1		= getRegister(regRsIndex);
	int operand2		= getRegister(regRdIndex);
	int result			= 0;

	// Execute
	switch(instruction & INSTR_ALU_OP) {

		// AND
		case INSTR_ALU_OP_AND :

			// Calculate result
			result = operand2 & operand1;

			// Set PSR flags
			setRegisterBit(REG_CPSR, PSR_FLAG_ZERO, result == 0);
			setRegisterBit(REG_CPSR, PSR_FLAG_NEGATIVE, result < 0);

			// Store result
			setRegister(regRdIndex, result);
			
			break;

		// EOR
		case INSTR_ALU_OP_EOR :

			// Calculate result
			result = operand2 ^ operand1;

			// Set PSR flags
			setRegisterBit(REG_CPSR, PSR_FLAG_ZERO, result == 0);
			setRegisterBit(REG_CPSR, PSR_FLAG_NEGATIVE, result < 0);

			// Store result
			setRegister(regRdIndex, result);
			
			break;

		// LSL
		case INSTR_ALU_OP_LSL :

			// Check for shift values of zero			
			if(operand1 == 0)
				result = operand2;

			// Non-zero shifts
			else {
					
				// Calculate result
				result = operand2 << operand1;

				// Set carry flag
				setRegisterBit(REG_CPSR, PSR_FLAG_CARRY, (operand2 & (1 << (32 - operand1))) != 0);
			}
			
			// Set PSR flags
			setRegisterBit(REG_CPSR, PSR_FLAG_ZERO, result == 0);
			setRegisterBit(REG_CPSR, PSR_FLAG_NEGATIVE, result < 0);
			
			// Store result
			setRegister(regRdIndex, result);
			
			break;

		// LSR
		case INSTR_ALU_OP_LSR :

			// Check for shift values of zero			
			if(operand1 == 0) {

				// Zero result
				result = 0;
				
				// Set carry flag
				setRegisterBit(REG_CPSR, PSR_FLAG_CARRY, (operand2 & 0x80000000) != 0);
			}

			// Non-zero shifts
			else {
						
				// Calculate result
				result = operand2 >>> operand1;

				// Set carry flag
				setRegisterBit(REG_CPSR, PSR_FLAG_CARRY, (operand2 & (1 << (operand1 - 1))) != 0);
			}
			
			// Set PSR flags
			setRegisterBit(REG_CPSR, PSR_FLAG_ZERO, result == 0);
			setRegisterBit(REG_CPSR, PSR_FLAG_NEGATIVE, result < 0);
			
			// Store result
			setRegister(regRdIndex, result);
			
			break;

		// ASR
		case INSTR_ALU_OP_ASR :
		
			// Check for shift values of zero			
			if(operand1 == 0) {

				// Set result
				result = (operand2 >> 16) >> 16;
				
				// Set carry flag
				setRegisterBit(REG_CPSR, PSR_FLAG_CARRY, (operand2 & (1 << (operand1 - 1))) != 0);
			}

			// Non-zero shifts
			else {
					
				// Calculate result
				result = operand2 >> operand1;

				// Set carry flag
				setRegisterBit(REG_CPSR, PSR_FLAG_CARRY, (operand2 & (1 << (operand1 - 1))) != 0);
			}
			
			// Set PSR flags
			setRegisterBit(REG_CPSR, PSR_FLAG_ZERO, result == 0);
			setRegisterBit(REG_CPSR, PSR_FLAG_NEGATIVE, result < 0);
			
			// Store result
			setRegister(regRdIndex, result);
			
			break;

		// ADC
		case INSTR_ALU_OP_ADC :

			// Calculate result
			result = operand2 + operand1 + (getRegisterBit(REG_CPSR, PSR_FLAG_CARRY) ? 1 : 0);

			// Set PSR flags
			setRegisterBit(REG_CPSR, PSR_FLAG_ZERO, result == 0);
			setRegisterBit(REG_CPSR, PSR_FLAG_NEGATIVE, result < 0);
			setAddCarryOverflowFlags(operand1, operand2, result);

			// Store result
			setRegister(regRdIndex, result);
			
			break;

		// SBC
		case INSTR_ALU_OP_SBC :

			// Calculate result
			result = operand2 - operand1 - (getRegisterBit(REG_CPSR, PSR_FLAG_CARRY) ? 0 : 1);

			// Set PSR flags
			setRegisterBit(REG_CPSR, PSR_FLAG_ZERO, result == 0);
			setRegisterBit(REG_CPSR, PSR_FLAG_NEGATIVE, result < 0);
			setSubCarryOverflowFlags(operand1, operand2, result);

			// Store result
			setRegister(regRdIndex, result);
			
			break;

		// ROR
		case INSTR_ALU_OP_ROR :
		
			// Check for shift values of zero			
			if(operand1 == 0) {

				// Get current carry flag
				boolean carry = getRegisterBit(REG_CPSR, PSR_FLAG_CARRY);
				
				// Set value
				result = operand2 >>> 1;
								
				// Set carry flag
				setRegisterBit(REG_CPSR, PSR_FLAG_CARRY, (operand2 & 0x00000001) != 0);
	
				// Rotate carry flag into result
				result |= carry ? 0x80000000 : 0x00000000;
			}

			// Non-zero shifts
			else {
					
				// Calculate result
				result = (operand2 >>> operand1) | (operand2 << (32 - operand1));

				// Set carry flag
				setRegisterBit(REG_CPSR, PSR_FLAG_CARRY, (operand2 & 0x80000000) != 0);
			}
			
			// Set PSR flags
			setRegisterBit(REG_CPSR, PSR_FLAG_ZERO, result == 0);
			setRegisterBit(REG_CPSR, PSR_FLAG_NEGATIVE, result < 0);
			
			// Store result
			setRegister(regRdIndex, result);
			
			break;

		// TST
		case INSTR_ALU_OP_TST :

			// Calculate result
			result = operand2 & operand1;

			// Set PSR flags
			setRegisterBit(REG_CPSR, PSR_FLAG_ZERO, result == 0);
			setRegisterBit(REG_CPSR, PSR_FLAG_NEGATIVE, result < 0);

			break;

		// NEG
		case INSTR_ALU_OP_NEG :

			// Calculate result
			result = -operand1;

			// Set PSR flags
			setRegisterBit(REG_CPSR, PSR_FLAG_ZERO, result == 0);
			setRegisterBit(REG_CPSR, PSR_FLAG_NEGATIVE, result < 0);
			setSubCarryOverflowFlags(operand1, 0, result);

			// Store result
			setRegister(regRdIndex, result);

			break;

		// CMP
		case INSTR_ALU_OP_CMP :

			// Calculate result
			result = operand2 - operand1;

			// Set PSR flags
			setRegisterBit(REG_CPSR, PSR_FLAG_ZERO, result == 0);
			setRegisterBit(REG_CPSR, PSR_FLAG_NEGATIVE, result < 0);
			setSubCarryOverflowFlags(operand1, operand2, result);

			break;

		// CMN
		case INSTR_ALU_OP_CMN :

			// Calculate result
			result = operand2 + operand1;

			// Set PSR flags
			setRegisterBit(REG_CPSR, PSR_FLAG_ZERO, result == 0);
			setRegisterBit(REG_CPSR, PSR_FLAG_NEGATIVE, result < 0);
			setAddCarryOverflowFlags(operand1, operand2, result);

			break;

		// ORR
		case INSTR_ALU_OP_ORR :

			// Calculate result
			result = operand2 | operand1;

			// Set PSR flags
			setRegisterBit(REG_CPSR, PSR_FLAG_ZERO, result == 0);
			setRegisterBit(REG_CPSR, PSR_FLAG_NEGATIVE, result < 0);

			// Store result
			setRegister(regRdIndex, result);
			
			break;
			
		// MUL
		case INSTR_ALU_OP_MUL :

			// Calculate result
			result = operand1 * operand2;

			// Set PSR flags
			setRegisterBit(REG_CPSR, PSR_FLAG_ZERO, result == 0);
			setRegisterBit(REG_CPSR, PSR_FLAG_NEGATIVE, result < 0);

			// Store result
			setRegister(regRdIndex, result);

			break;
			
		// BIC
		case INSTR_ALU_OP_BIC :

			// Calculate result
			result = operand2 & ~operand1;

			// Set PSR flags
			setRegisterBit(REG_CPSR, PSR_FLAG_ZERO, result == 0);
			setRegisterBit(REG_CPSR, PSR_FLAG_NEGATIVE, result < 0);

			// Store result
			setRegister(regRdIndex, result);

			break;
			
		// MVN
		case INSTR_ALU_OP_MVN :

			// Calculate result
			result = ~operand1;

			// Set PSR flags
			setRegisterBit(REG_CPSR, PSR_FLAG_ZERO, result == 0);
			setRegisterBit(REG_CPSR, PSR_FLAG_NEGATIVE, result < 0);

			// Store result
			setRegister(regRdIndex, result);

			break;
			
	}
	
	// Return number of cycles
	return 3;
}
private final int instrThumbASR(short instruction) {

	// Get offset
	int shiftValue = (instruction & INSTR_LSL_OFFSET) >>> 6;

	// Get source and destination registers
	int regRsIndex	= (instruction & INSTR_MASK_R_03_05) >> INSTR_SHFT_R_03_05;
	int regRdIndex	= (instruction & INSTR_MASK_R_00_02) >> INSTR_SHFT_R_00_02;

	// Get value
	int value = getRegister(regRsIndex);
	int result = 0;

	// Apply shift
	if(shiftValue == 0) {

		// Set result
		result = (value >> 16) >> 16;
		
		// Set carry flag
		setRegisterBit(REG_CPSR, PSR_FLAG_CARRY, (value & (1 << (shiftValue - 1))) != 0);
	}

	// Non-zero shifts
	else {
			
		// Calculate result
		result = value >> shiftValue;

		// Set carry flag
		setRegisterBit(REG_CPSR, PSR_FLAG_CARRY, (value & (1 << (shiftValue - 1))) != 0);
	}
	
	// Set PSR flags
	setRegisterBit(REG_CPSR, PSR_FLAG_ZERO, result == 0);
	setRegisterBit(REG_CPSR, PSR_FLAG_NEGATIVE, result < 0);
	
	// Store result in register
	setRegister(regRdIndex, result);
	
	// Return number of cycles
	return 3;
}
private final int instrThumbBRANCH(short instruction) {

	// Fetch register Rn
	int offset 	= ((instruction & INSTR_BRANCH_OFFSET) << 5) >>> 4;

	// Sign extend offset
	if((offset & 0x0800) != 0)
		offset |= 0xfffff800;
		
	// Add offset to PC
	setRegister(REG_PC, getRegister(REG_PC) + offset + INSTR_SIZE_THUMB);
	
	// Return number of cycles
	return 3;	
}
private final int instrThumbCBRANCH(short instruction) {

	// Fetch register Rn
	int offset 	= ((instruction & INSTR_CBRANCH_OFFSET) << 8) >>> 7;
	int cond	= (instruction & INSTR_CBRANCH_COND) >>> 8;

	// Sign extend offset
	if((offset & 0x0100) != 0)
		offset |= 0xffffff00;

	// Check condition
	if(checkCondition((cond & 0x0000000f) << 4)) {

		// Add offset to PC
		setRegister(REG_PC, getRegister(REG_PC) + offset + INSTR_SIZE_THUMB);
	}
	
	// Return number of cycles
	return 3;	
}
private final int instrThumbHIREG(short instruction) {

	// Get source and destination registers
	int regRsIndex		= ((instruction & INSTR_MASK_R_03_05) >> INSTR_SHFT_R_03_05) + ((instruction & INSTR_HIREG_OP_H2) != 0 ? 8 : 0);
	int regRdIndex		= ((instruction & INSTR_MASK_R_00_02) >> INSTR_SHFT_R_00_02) + ((instruction & INSTR_HIREG_OP_H1) != 0 ? 8 : 0);

	// Get operands
	int operand1		= getRegister(regRdIndex);
	int operand2		= getRegister(regRsIndex);
	int result			= 0;

	// Adjust operands for PC
	if(regRdIndex == REG_PC)
		operand1 += INSTR_SIZE_THUMB;
	if(regRsIndex == REG_PC)
		operand2 += INSTR_SIZE_THUMB;

	// Execute op
	switch(instruction & INSTR_HIREG_OP) {

		// Add
		case INSTR_HIREG_OP_ADD :
		
			// Calculate result
			result = operand1 + operand2;

			// Store result
			setRegister(regRdIndex, result);
			
			break;

		// Compare
		case INSTR_HIREG_OP_CMP :
		
			// Calculate result
			result = operand1 - operand2;

			// Set PSR flags
			setRegisterBit(REG_CPSR, PSR_FLAG_ZERO, result == 0);
			setRegisterBit(REG_CPSR, PSR_FLAG_NEGATIVE, result < 0);
			setSubCarryOverflowFlags(operand1, operand2, result);
						
			break;

		// Move
		case INSTR_HIREG_OP_MOV :
		
			// Calculate result
			result = operand2;
			
			// Store result
			setRegister(regRdIndex, result);
			
			break;

		// Branch Exchange
		case INSTR_HIREG_OP_BX :
		
			// Store value in PC
			setRegister(REG_PC, operand2 & 0xfffffffe);

			// Store state bit
			setRegisterBit(REG_CPSR, PSR_FLAG_STATE, (operand2 & 0x00000001) != 0);
			
			break;
	}

	// Return number of cycles
	return 3;
}
private final int instrThumbLBRANCH(short instruction) {

	// Get HI flag
	boolean high = (instruction & INSTR_LBRANCH_HI) != 0;
	
	// Fetch register Rn
	int offset 	= (instruction & INSTR_LBRANCH_OFFSET);

	// Handle high/low
	if(high) {

		// Save next instruction address
		int temp = getRegister(REG_PC);
		
		// Load PC
		setRegister(REG_PC, getRegister(REG_LR) + (offset << 1) + INSTR_SIZE_THUMB);

		// Save old PC in LR
		setRegister(REG_LR, temp | 0x00000001);
	}
	else {

		// Sign extend
		offset = ((offset << 21) >> 21) << 12;

		// Store result in LR
		setRegister(REG_LR, getRegister(REG_PC) + offset);
	}
	
	// Return number of cycles
	return 3;	
}
private final int instrThumbLDR(short instruction) {

	// Get register indices
	int regRoIndex		= (instruction & INSTR_MASK_R_06_08) >> INSTR_SHFT_R_06_08;
	int regRbIndex		= (instruction & INSTR_MASK_R_03_05) >> INSTR_SHFT_R_03_05;
	int regRdIndex		= (instruction & INSTR_MASK_R_00_02) >> INSTR_SHFT_R_00_02;

	// Get flags
	boolean byteOp		= (instruction & INSTR_TLDR_BYTE) != 0;
	boolean load		= (instruction & INSTR_TLDR_LOAD) != 0;

	// Get base offset
	int baseOffset		= getRegister(regRbIndex);

	// Get offset
	int offset = getRegister(regRoIndex);

	// If pre-indexing is active, add offset first
	baseOffset += offset;
		
	// Handle load operations
	if(load) {

		if(byteOp)
			setRegister(regRdIndex, 0xff & memory.readByte(baseOffset));
		else// if((baseOffset & 0x00000003) == 0)
			setRegister(regRdIndex, memory.readWord(baseOffset));
	}

	// Handle write operations
	else {

		// Read register value
		int value = getRegister(regRdIndex);

		// Write to memory
		if(byteOp)
			memory.writeByte((byte)(0xff & value), baseOffset);
		else
			memory.writeWord(value, baseOffset);
	}
			
	// Return number of cycles
	return 3;
}
private final int instrThumbLDRH(short instruction) {

	// Get register indices
	int regRbIndex		= (instruction & INSTR_MASK_R_03_05) >> INSTR_SHFT_R_03_05;
	int regRdIndex		= (instruction & INSTR_MASK_R_00_02) >> INSTR_SHFT_R_00_02;

	// Get flags
	boolean load		= (instruction & INSTR_TLDRH_LOAD) != 0;

	// Get base offset
	int baseOffset		= getRegister(regRbIndex);

	// Get offset
	int offset = (instruction & INSTR_TLDRH_OFFSET) >>> 5;

	// If pre-indexing is active, add offset first
	baseOffset += offset;
		
	// Handle loads
	if(load)
		setRegister(regRdIndex, 0xffff & memory.readHalfWord(baseOffset));
		
	// Handle stores
	else {
		
		// Read register value
		int value = getRegister(regRdIndex);

		// Write to memory
		memory.writeHalfWord((short)(0xffff & value), baseOffset);
	}
			
	// Return number of cycles
	return 3;
}
private final int instrThumbLDRHS(short instruction) {

	// Get register indices
	int regRoIndex		= (instruction & INSTR_MASK_R_06_08) >> INSTR_SHFT_R_06_08;
	int regRbIndex		= (instruction & INSTR_MASK_R_03_05) >> INSTR_SHFT_R_03_05;
	int regRdIndex		= (instruction & INSTR_MASK_R_00_02) >> INSTR_SHFT_R_00_02;

	// Get flags
	boolean halfword	= (instruction & INSTR_LDRHS_HALFWORD) != 0;
	boolean sign		= (instruction & INSTR_LDRHS_SIGN) != 0;

	// Get base offset
	int baseOffset		= getRegister(regRbIndex);

	// Get offset
	int offset = getRegister(regRoIndex);

	// If pre-indexing is active, add offset first
	baseOffset += offset;
		
	// Handle sign extended loads
	if(sign) {

		// Handle halfword store
		if(halfword) {

			// Read value from memory
			int value = memory.readHalfWord(baseOffset) & 0x0000ffff;

			// Sign extend
			if((value & 0x00008000) != 0)
				value |= 0xffff0000;

			// Store value
			setRegister(regRdIndex, value);
		}
		else {

			// Read value from memory
			int value = memory.readByte(baseOffset) & 0x000000ff;

			// Sign extend
			if((value & 0x00000080) != 0)
				value |= 0xffffff00;

			// Store value
			setRegister(regRdIndex, value);
		}
	}

	// Handle non sign-extended load/store
	else {

		// Get value
		int value = getRegister(regRdIndex);

		// Handle halfword store
		if(halfword)
			memory.writeHalfWord((short)(0xffff & value), baseOffset);
		else
			setRegister(regRdIndex, memory.readHalfWord(baseOffset) & 0x0000ffff);
	}
			
	// Return number of cycles
	return 3;
}
private final int instrThumbLDRI(short instruction) {

	// Get register indices
	int regRbIndex		= (instruction & INSTR_MASK_R_03_05) >> INSTR_SHFT_R_03_05;
	int regRdIndex		= (instruction & INSTR_MASK_R_00_02) >> INSTR_SHFT_R_00_02;

	// Get flags
	boolean byteOp		= (instruction & INSTR_LDRI_BYTE) != 0;
	boolean load		= (instruction & INSTR_LDRI_LOAD) != 0;

	// Get base offset
	int baseOffset		= getRegister(regRbIndex);

	// Get offset
	int offset = ((instruction & INSTR_LDRI_OFFSET) >>> 6) << (byteOp ? 0 : 2);

	// If pre-indexing is active, add offset first
	baseOffset += offset;
		
	// Handle loads
	if(load) {
		
		if(byteOp)
			setRegister(regRdIndex, 0xff & memory.readByte(baseOffset));
		else// if((baseOffset & 0x00000003) == 0)
			setRegister(regRdIndex, memory.readWord(baseOffset));
	}

	// Handle stores
	else {

		// Read register value
		int value = getRegister(regRdIndex);

		// Write to memory
		if(byteOp)
			memory.writeByte((byte)(0xff & value), baseOffset);
		else
			memory.writeWord(value, baseOffset);
	}
			
	// Return number of cycles
	return 3;
}
private final int instrThumbLOADADDR(short instruction) {

	// Get offset
	int offset = (instruction & INSTR_LOADADDR_OFFSET) << 2;

	// Get load SP flag
	boolean sp = (instruction & INSTR_LOADADDR_SP) != 0;
	
	// Get immediate value and add it
	offset = (sp ? getRegister(REG_SP) : getRegister(REG_PC) + INSTR_SIZE_THUMB) + offset;
	
	// Get value from memory and store it
	setRegister((instruction & INSTR_MASK_R_08_10) >> INSTR_SHFT_R_08_10, offset);	
	
	// Return number of cycles
	return 3;
}
private final int instrThumbLSL(short instruction) {

	// Get offset
	int shiftValue = (instruction & INSTR_LSL_OFFSET) >>> 6;

	// Get source and destination registers
	int regRsIndex	= (instruction & INSTR_MASK_R_03_05) >> INSTR_SHFT_R_03_05;
	int regRdIndex	= (instruction & INSTR_MASK_R_00_02) >> INSTR_SHFT_R_00_02;

	// Get value
	int value = getRegister(regRsIndex);
	int result = 0;

	// Apply shift
	if(shiftValue == 0)
		result = value;

	// Non-zero shifts
	else {
			
		// Calculate result
		result = value << shiftValue;

		// Set carry flag
		setRegisterBit(REG_CPSR, PSR_FLAG_CARRY, (value & (1 << (32 - shiftValue))) != 0);
	}
	
	// Set PSR flags
	setRegisterBit(REG_CPSR, PSR_FLAG_ZERO, result == 0);
	setRegisterBit(REG_CPSR, PSR_FLAG_NEGATIVE, result < 0);
	
	// Store result in register
	setRegister(regRdIndex, result);
	
	// Return number of cycles
	return 3;
}
private final int instrThumbLSR(short instruction) {

	// Get offset
	int shiftValue = (instruction & INSTR_LSL_OFFSET) >>> 6;

	// Get source and destination registers
	int regRsIndex	= (instruction & INSTR_MASK_R_03_05) >> INSTR_SHFT_R_03_05;
	int regRdIndex	= (instruction & INSTR_MASK_R_00_02) >> INSTR_SHFT_R_00_02;

	// Get value
	int value = getRegister(regRsIndex);
	int result = 0;

	// Apply shift
	if(shiftValue == 0) {

		// Zero result
		result = 0;
		
		// Set carry flag
		setRegisterBit(REG_CPSR, PSR_FLAG_CARRY, (value & 0x80000000) != 0);
	}

	// Non-zero shifts
	else {
				
		// Calculate result
		result = value >>> shiftValue;

		// Set carry flag
		setRegisterBit(REG_CPSR, PSR_FLAG_CARRY, (value & (1 << (shiftValue - 1))) != 0);
	}
	
	// Set PSR flags
	setRegisterBit(REG_CPSR, PSR_FLAG_ZERO, result == 0);
	setRegisterBit(REG_CPSR, PSR_FLAG_NEGATIVE, result < 0);
	
	// Store result in register
	setRegister(regRdIndex, result);
	
	// Return number of cycles
	return 3;
}
private final int instrThumbMOV(short instruction) {

	// Get immediate offset
	int operand2		= instruction & INSTR_MOV_OFFSET;
	int result			= 0;
	
	// Get source and destination registers
	int regRdIndex		= (instruction & INSTR_MASK_R_08_10) >> INSTR_SHFT_R_08_10;
	
	// Get operands
	int operand1 		= getRegister(regRdIndex);

	// Do op
	switch(instruction & INSTR_MOV_OP) {

		// Move
		case INSTR_MOV_OP_MOV :
		
			// Get result
			result = operand2;
	
			// Set PSR flags		
			setRegisterBit(REG_CPSR, PSR_FLAG_ZERO, result == 0);
			setRegisterBit(REG_CPSR, PSR_FLAG_NEGATIVE, result < 0);
			
			// Store result
			setRegister(regRdIndex, result);
			
			break;

		// Compare
		case INSTR_MOV_OP_CMP :
		
			// Calculate result
			result = operand1 - operand2;

			// Set PSR flags
			setRegisterBit(REG_CPSR, PSR_FLAG_ZERO, result == 0);
			setRegisterBit(REG_CPSR, PSR_FLAG_NEGATIVE, result < 0);
			setSubCarryOverflowFlags(operand1, operand2, result);
			
			break;

		// Add
		case INSTR_MOV_OP_ADD :
		
			// Calculate result
			result = operand1 + operand2;

			// Set PSR flags
			setRegisterBit(REG_CPSR, PSR_FLAG_ZERO, result == 0);
			setRegisterBit(REG_CPSR, PSR_FLAG_NEGATIVE, result < 0);
			setAddCarryOverflowFlags(operand1, operand2, result);
			
			// Store result
			setRegister(regRdIndex, result);
			
			break;

		// Substract
		case INSTR_MOV_OP_SUB :
		
			// Calculate result
			result = operand1 - operand2;

			// Set PSR flags
			setRegisterBit(REG_CPSR, PSR_FLAG_ZERO, result == 0);
			setRegisterBit(REG_CPSR, PSR_FLAG_NEGATIVE, result < 0);
			setSubCarryOverflowFlags(operand1, operand2, result);
			
			// Store result
			setRegister(regRdIndex, result);
			
			break;
	}
		
	// Return number of cycles
	return 3;
}
private final int instrThumbMULTLS(short instruction) {

	// Get register indices
	int regRnIndex		= (instruction & INSTR_MASK_R_08_10) >> INSTR_SHFT_R_08_10;
	
	// Get flags
	boolean load		= (instruction & INSTR_MULTLS_LOAD) != 0;
	boolean preIndexing	= false;
	boolean upOffset	= true;
	boolean writeBack	= true;
	
	// Get base offset
	int baseOffset		= getRegister(regRnIndex) & 0xfffffffc;

	// Initialise active bit value
	int activeRegister	= upOffset ? 1 : (1 << 7);

	// Write/load registers
	for (int i = 0; i < 8; i++){		
		
		// Check if register should be loaded/stored
		if((instruction & activeRegister) != 0) {

			// Get register index
			int registerIndex = upOffset ? i : (7 - i);

			// If pre-indexing is active adjust offset first
			if(preIndexing)
				baseOffset += upOffset ? 4 : -4;

			// Load/write data
			if(load)
				setRegister(registerIndex, memory.readWord(baseOffset));
			else
				memory.writeWord(getRegister(registerIndex), baseOffset);
				
			// If post-indexing is active adjust offset now
			if(!preIndexing)
				baseOffset += upOffset ? 4 : -4;
		}

		// Set next register bit
		if(upOffset)
			activeRegister <<= 1;
		else
			activeRegister >>>= 1;
	}

	// Do write back if needed
	if(writeBack || !preIndexing)
		setRegister(regRnIndex, baseOffset);
		
	// Return number of cycles
	return 3;
}
private final int instrThumbPCLOAD(short instruction) {

	// Get offset
	int offset = getRegister(REG_PC) + INSTR_SIZE_THUMB;

	// Get immediate value and add it
	offset += (instruction & INSTR_PCLOAD_IMM) << 2;
	
	// Get value from memory and store it
	setRegister((instruction & INSTR_MASK_R_08_10) >> INSTR_SHFT_R_08_10, memory.readWord(offset & 0xfffffffc));	
	
	// Return number of cycles
	return 3;
}
private final int instrThumbPUSHPOP(short instruction) {

	// Get register indices
	int regRnIndex		= REG_SP;
	
	// Get flags
	boolean load		= (instruction & INSTR_PUSHPOP_LOAD) != 0;
	boolean preIndexing	= !load;
	boolean upOffset	= load;
	boolean writeBack	= true;

	// Get base offset
	int baseOffset		= getRegister(regRnIndex) & 0xfffffffc;

	// Initialise active bit value
	int activeRegister	= upOffset ? 1 : (1 << 8);

	// Write/load registers
	for (int i = 0; i < 9; i++){		
		
		// Check if register should be loaded/stored
		if((instruction & activeRegister) != 0) {

			// Get register index
			int registerIndex = upOffset ? i : (8 - i);

			// Adjust if pclr
			if(registerIndex == 8)
				registerIndex = load ? REG_PC : REG_LR;

			// If pre-indexing is active adjust offset first
			if(preIndexing)
				baseOffset += upOffset ? 4 : -4;

			// Load/write data
			if(load){
				if(registerIndex==REG_PC)
					setRegister(registerIndex, memory.readWord(baseOffset)-1);
				else
					setRegister(registerIndex, memory.readWord(baseOffset));
			}else
				memory.writeWord(getRegister(registerIndex), baseOffset);
				
			// If post-indexing is active adjust offset now
			if(!preIndexing)
				baseOffset += upOffset ? 4 : -4;
		}

		// Set next register bit
		if(upOffset)
			activeRegister <<= 1;
		else
			activeRegister >>>= 1;
	}

	// Do write back if needed
	if(writeBack || !preIndexing)
		setRegister(regRnIndex, baseOffset);
		
	// Return number of cycles
	return 3;
}
private final int instrThumbSPADD(short instruction) {

	// Get offset
	int offset = (instruction & INSTR_SPADD_OFFSET) << 2;

	// Get immediate value and add it
	setRegister(REG_SP, getRegister(REG_SP) + (((instruction & INSTR_SPADD_SIGN) != 0) ? -offset : offset));
		
	// Return number of cycles
	return 3;
}
private final int instrThumbSPLOAD(short instruction) {

	// Get offset
	int offset = getRegister(REG_SP);

	// Get immediate value and add it
	offset += (instruction & INSTR_PCLOAD_IMM) << 2;
	
	// Get value from memory and store it
	if((instruction & INSTR_SPLOAD_LOAD) != 0)
		setRegister((instruction & INSTR_MASK_R_08_10) >> INSTR_SHFT_R_08_10, memory.readWord(offset & 0xfffffffc));
	else
		memory.writeWord(getRegister((instruction & INSTR_MASK_R_08_10) >> INSTR_SHFT_R_08_10), offset & 0xfffffffc);
	
	// Return number of cycles
	return 3;
}
private final int instrThumbSWI(short instruction) {

	// Get current CPSR value
	int currentPSR = getRegister(REG_CPSR);

	// Save current PC is LR_svc
	registers[PSR_MODE_SUPERVISOR][REG_SPSR] = getRegister(REG_CPSR);
	registers[PSR_MODE_SUPERVISOR][REG_LR] = getRegister(REG_PC);
	
	// Move to supervisor mode and force ARM mode
	setRegister(REG_CPSR, ((currentPSR & 0xffffffc0) & ~PSR_FLAG_STATE) | PSR_MODE_SUPERVISOR);
	
	// Force PC to exception table entry point
	setRegister(REG_PC, 0x00000008);
	
	// Return number of cycles
	return 4;	
}
public void interrupt() {
	
	// Get current CPSR value
	int currentPSR = getRegister(REG_CPSR);

	// Save current PC is LR_svc
	registers[PSR_MODE_IRQ][REG_SPSR] = getRegister(REG_CPSR);
	registers[PSR_MODE_IRQ][REG_LR] = getRegister(REG_PC);
	
	// Move to supervisor mode and force ARM mode
	setRegister(REG_CPSR, ((currentPSR & 0xffffffc0) & ~PSR_FLAG_STATE) | PSR_MODE_IRQ);
	
	// Force PC to exception table entry point
	setRegister(REG_PC, 0x00000018);
}
public void reset() {

	// Clear all registers
	for(int i = 0; i < registers.length; i++)
		for(int j = 0; j < registers[i].length; j++)
			registers[i][j] = 0;
		
	if(currentRegisters != null)	
		for(int i = 0; i < currentRegisters.length; i++)
			currentRegisters[i] = 0;

	// Set CSPR flags
	setRegister(REG_CPSR, PSR_MODE_SUPERVISOR | PSR_FLAG_FIQDISABLE | PSR_FLAG_IRQDISABLE);
	setOperatingMode(PSR_MODE_USER, PSR_MODE_SUPERVISOR);

	// Set Stack Pointer
	setRegister(REG_SP, 0x03007f00);

	// Set Program Counter
	setRegister(REG_LR, 0x08000000);
	setRegister(REG_PC, 0x08000000);

	// Reset cycle counter
	cycleCount = 0;
}
public void run() {

	// Main execution loop
	while(!haltExecution) {

		// Execute next instruction
		executeNextInstruction();
	}
}
private void setAddCarryOverflowFlags(int operand1, int operand2, int result) {

	// Get nag flags of operands and result
	boolean negOp1 = operand1 < 0;
	boolean negOp2 = operand2 < 0;
	boolean negRes = result < 0;

	// Set carry and overflow
	setRegisterBit(REG_CPSR, PSR_FLAG_CARRY, (negOp1 && negOp2) || (negOp1 && !negRes) || (negOp2 && !negRes));
	setRegisterBit(REG_CPSR, PSR_FLAG_OVERFLOW, (negOp1 && negOp2 && !negRes) || (!negOp1 && !negOp2 && negRes));
}    
public void setOperatingMode(int currentMode, int requestMode) {

	// Set system to user mode
	if(currentMode == PSR_MODE_SYSTEM)
		currentMode = PSR_MODE_USER;
	if(requestMode == PSR_MODE_SYSTEM)
		requestMode = PSR_MODE_USER;

	// Copy registers from current bank to old mode bank if needed
	if(currentMode == PSR_MODE_FIQ)
		System.arraycopy(currentRegisters, REG_R8, registers[currentMode], REG_R8, REG_R12 - REG_R8 + 1);

	registers[currentMode][REG_R13] 	= currentRegisters[REG_R13];
	registers[currentMode][REG_R14] 	= currentRegisters[REG_R14];
	registers[currentMode][REG_SPSR] 	= currentRegisters[REG_SPSR];

	// Copy registers from requested bank to current bank
	if(requestMode == PSR_MODE_FIQ)
		System.arraycopy(registers[requestMode], REG_R8, currentRegisters, REG_R8, REG_R12 - REG_R8 + 1);

	currentRegisters[REG_R13]				= registers[requestMode][REG_R13];
	currentRegisters[REG_R14]				= registers[requestMode][REG_R14];
	currentRegisters[REG_SPSR]				= registers[requestMode][REG_SPSR];	
}
public void setRegister(int registerIndex, int value) {

	// Detect mode changes
	if(registerIndex == REG_CPSR) {

		// Get current and requested mode
		int currentMode = getRegister(REG_CPSR) & PSR_MODE;
		int requestMode = value & PSR_MODE;

		// If modes are not equal change current register bank
		if(currentMode != requestMode)
			setOperatingMode(currentMode, requestMode);
	}
	
	// Return register value
	currentRegisters[registerIndex] = value;
}
public void setRegisterBit(int registerIndex, int bit, boolean state) {

	// Set bit
	if(state)
		setRegister(registerIndex, getRegister(registerIndex) | bit);
	else
		setRegister(registerIndex, getRegister(registerIndex) & ~bit);
}
private void setSubCarryOverflowFlags(int operand1, int operand2, int result) {

	// Get nag flags of operands and result
	boolean negOp1 = operand1 < 0;
	boolean negOp2 = operand2 < 0;
	boolean negRes = result < 0;

	// Set carry and overflow
	setRegisterBit(REG_CPSR, PSR_FLAG_CARRY, (negOp1 && !negOp2) || (negOp1 && !negRes) || (!negOp2 && !negRes));
	setRegisterBit(REG_CPSR, PSR_FLAG_OVERFLOW, (negOp1 && !negOp2 && !negRes) || (!negOp1 && negOp2 && negRes));
}    
private int shift(int registerIndex, int shift, boolean setConditionFlags) {

	// Get register value
	int value = getRegister(registerIndex);

	// Account for prefetch
	if(registerIndex == REG_PC) {

		// Add 8 bytes
		value += INSTR_SIZE_ARM;
		
		// Account for register specified shifts using PC
		if((shift & SHIFT_OP_MASK) != 0) {
			value += INSTR_SIZE_ARM;

			System.out.println("SHIFT AMOUNT IS REG SPECIFIED AND PC, INACCURATE ADJUST!");
		}
	}

	
	// Initialise result
	int result = 0;

	// Get shift type
	int shiftType = shift & SHIFT_TYPE;
	int shiftValue;

	// Immediate shift
	if((shift & SHIFT_OP_MASK) == 0) {

		// Calculate shift value
		shiftValue = (shift & SHIFT_AMOUNT_VALUE) >> SHIFT_AMOUNT_SHIFT;
	}
	
	// Register shift
	else {

		// Get register index
		int regRsIndex = (shift & SHIFT_REG_INDEX) >> SHIFT_REG_SHIFT;

		// Get shift value
		shiftValue = getRegister(regRsIndex) & 0x0f;
	}

		  //else if (shiftAmount > 32)
		  //{
			//if (shiftType == armStateDataProcessingLogicalLeftBits)
			//{
			  //tmpCPSR.setOff(cFlagBit);
			  //operand2 = 0;
			//}
			//else if (shiftType == armStateDataProcessingLogicalRightBits)
			//{
			  //tmpCPSR.setOff(cFlagBit);
			  //operand2 = 0;
			//}
			//else if (shiftType == armStateDataProcessingArithmRightBits)
			//{
			  //tmpCPSR.setBit(cFlagBit, ((operand2 & 0x80000000) != 0));
			  //operand2 >>= 16;
			  //operand2 >>= 16;
			//}
			//else if (shiftType == armStateDataProcessingRotateRightBits)
			//{
			  //shiftAmount = ((shiftAmount - 1) & 0x1f) + 1; // put shiftAmount in the range [1..32]
			  //operand2 = (operand2 >>> shiftAmount) | (operand2 << (32 - shiftAmount));
			  //tmpCPSR.setBit(cFlagBit, ((operand2 & 0x80000000) != 0));
			//}

		  
	// Calculate shifts
	switch(shiftType) {

		// Logical shift left (LSL)
		case SHIFT_LSL :

			// Check for shift values of zero			
			if(shiftValue == 0)
				result = value;

			// Large shifts
			else if(shiftValue > 32) {

				// Turn of carry
			  	setRegisterBit(REG_CPSR, PSR_FLAG_CARRY, false);

			  	// Clear result
			  	result = 0;
			}
			
			// Non-zero shifts
			else {
					
				// Calculate result
				result = value << shiftValue;

				// Set carry flag
				if(setConditionFlags)
					setRegisterBit(REG_CPSR, PSR_FLAG_CARRY, (value & (1 << (32 - shiftValue))) != 0);
			}
			
			break;

		// Logical shift left (LSR)
		case SHIFT_LSR :
		
			// Check for shift values of zero			
			if(shiftValue == 0) {

				// Zero result
				result = 0;
				
				// Set carry flag
				if(setConditionFlags)
					setRegisterBit(REG_CPSR, PSR_FLAG_CARRY, (value & 0x80000000) != 0);
			}

			// Large shifts
			else if(shiftValue > 32) {

				// Turn of carry
			  	setRegisterBit(REG_CPSR, PSR_FLAG_CARRY, false);

			  	// Clear result
			  	result = 0;
			}
			
			// Non-zero shifts
			else {
						
				// Calculate result
				result = value >>> shiftValue;

				// Set carry flag
				if(setConditionFlags)
					setRegisterBit(REG_CPSR, PSR_FLAG_CARRY, (value & (1 << (shiftValue - 1))) != 0);
			}
			
			break;

		// Logical shift left (ASR)
		case SHIFT_ASR :
		
			// Check for shift values of zero			
			if(shiftValue == 0) {

				// Set result
				result = (value >> 16) >> 16;
				
				// Set carry flag
				if(setConditionFlags)
					setRegisterBit(REG_CPSR, PSR_FLAG_CARRY, (value & (1 << (shiftValue - 1))) != 0);
			}

			// Large shifts
			else if(shiftValue > 32) {

				// Set carry
			  	setRegisterBit(REG_CPSR, PSR_FLAG_CARRY, ((value & 0x80000000) != 0));

			  	// Shift result
			  	result >>= 16;
			  	result >>= 16;
			}
			
			// Non-zero shifts
			else {
					
				// Calculate result
				result = value >> shiftValue;

				// Set carry flag
				if(setConditionFlags)
					setRegisterBit(REG_CPSR, PSR_FLAG_CARRY, (value & (1 << (shiftValue - 1))) != 0);
			}
			
			break;

		// Logical shift left (ROR)
		case SHIFT_ROR :
		
			// Check for shift values of zero			
			if(shiftValue == 0) {

				// Get current carry flag
				boolean carry = getRegisterBit(REG_CPSR, PSR_FLAG_CARRY);
				
				// Set value
				result = value >>> 1;
								
				// Set carry flag
				if(setConditionFlags)
					setRegisterBit(REG_CPSR, PSR_FLAG_CARRY, (value & 0x00000001) != 0);
	
				// Rotate carry flag into result
				result |= carry ? 0x80000000 : 0x00000000;
			}
			// Large shifts
			else if(shiftValue > 32) {

				// Get shift value
			  	shiftValue = ((shiftValue - 1) & 0x1f) + 1;

			  	// Get result
			  	result = (value >>> shiftValue) | (value << (32 - shiftValue));
			  	
				// Set carry flag
				if(setConditionFlags)
					setRegisterBit(REG_CPSR, PSR_FLAG_CARRY, (value & (1 << (shiftValue - 1))) != 0);
			}
			
			// Non-zero shifts
			else {
					
				// Calculate result
				result = (value >>> shiftValue) | (value << (32 - shiftValue));

				// Set carry flag
				if(setConditionFlags)
					setRegisterBit(REG_CPSR, PSR_FLAG_CARRY, (value & 0x80000000) != 0);
			}
			
			break;
	}
	
	// Return result
	return result;
}
public void start() {

	// Create core thread
	if(thread == null) {

		thread = new Thread(this, "ARM7TDMI Core");
		thread.start();
	}
}
public void stop() {

	// Stop core thread
	if(thread != null) {

		thread.stop();
		thread = null;
	}
}
public String toHexString(int value, int length) {


	String s = Integer.toHexString(value);

	while(s.length() < length)
		s = "0" + s;

	s = "0x" + s;
	
	return s;
}
}
