package javagba;

public interface IORegConstants {

	// Register addresses
	public final static int REG_DISPCNT = 0x00000000; // Display Status

	public final static int REG_DISPSTAT = 0x00000004; // General LCD Status

	public final static int REG_VCOUNT = 0x00000006; // V Counter

	public final static int REG_BG0CNT = 0x00000008; // BG0 Control

	public final static int REG_BG1CNT = 0x0000000a; // BG0 Control

	public final static int REG_BG2CNT = 0x0000000c; // BG0 Control

	public final static int REG_BG3CNT = 0x0000000e; // BG0 Control

	public final static int REG_BG0HOFS = 0x00000010; // BG0 Horizontal Offset

	public final static int REG_BG0VOFS = 0x00000012; // BG0 Vertical Offset

	public final static int REG_MOSAIC = 0x0000004c; // Mosaic Status

	public final static int REG_DM0SAD_L = 0x000000b0; // DMA 0 Source Address
														// Low

	public final static int REG_DM0SAD_H = 0x000000b2; // DMA 0 Source Address
														// High

	public final static int REG_DM0DAD_L = 0x000000b4; // DMA 0 Destination
														// Address Low

	public final static int REG_DM0DAD_H = 0x000000b6; // DMA 0 Destination
														// Address High

	public final static int REG_DM0CNT_L = 0x000000b8; // DMA 0 Word Count Low

	public final static int REG_DM0CNT_H = 0x000000ba; // DMA 0 Word Count High

	public final static int REG_DM3CNT_H = 0x000000de; // DMA 3 Word Count High

	public final static int REG_P1 = 0x00000130; // Key Status

	public final static int REG_P1CNT = 0x00000132; // Key Interrupt Status

	public final static int REG_IRQ_ENABLE = 0x00000200; // Interrupt Enable

	public final static int REG_IRQ_REQUEST = 0x00000202; // Interrupt Request

	public final static int REG_IRQ_MASTER = 0x00000208; // Interrupt Master
															// Enable

	// DISPCNT flags/masks/shifts
	public final static int DISPCNT_OBJWIN_F = 0x8000; // OBJ Window Display
														// flag

	public final static int DISPCNT_WINDOW1_F = 0x4000; // Window 1 Display flag

	public final static int DISPCNT_WINDOW0_F = 0x2000; // Window 0 Display flag

	public final static int DISPCNT_DISP_OBJ_F = 0x1000; // OBJ Display flag

	public final static int DISPCNT_DISP_BG3_F = 0x0800; // BG3 Display flag

	public final static int DISPCNT_DISP_BG2_F = 0x0400; // BG2 Display flag

	public final static int DISPCNT_DISP_BG1_F = 0x0200; // BG1 Display flag

	public final static int DISPCNT_DISP_BG0_F = 0x0100; // BG0 Display flag

	public final static int DISPCNT_FORCEBLANK_F = 0x0080; // Forced Blank flag

	public final static int DISPCNT_OBJCHARMAP_F = 0x0040; // OBJ Character
															// VRAM Mapping
															// Format flag

	public final static int DISPCNT_HBLANKOBJINT_F = 0x0020; // H-Blank
																// Interval OBJ
																// Processing
																// flag

	public final static int DISPCNT_DISPLAYFRAME_F = 0x0010; // Display Frame
																// Selection

	public final static int DISPCNT_BGMODE_M = 0x0007; // BG Mode mask

	// DISPSTAT flags/masks/shifts
	public final static int DISPSTAT_VCOUNT_M = 0xff00; // V Count Setting mask

	public final static int DISPSTAT_VCOUNT_S = 8; // V Count Setting shift

	public final static int DISPSTAT_VCOUNT_INT_F = 0x0020; // V Counter Match
															// Interrupt Request
															// flag

	public final static int DISPSTAT_HBLANK_INT_F = 0x0010; // H-Blank Interrupt
															// Request flag

	public final static int DISPSTAT_VBLANK_INT_F = 0x0008; // V-Blank Interrupt
															// Request flag

	public final static int DISPSTAT_VCOUNT_EVAL_F = 0x0004; // V Counter
																// Evaluation
																// flag (true =
																// match, false
																// = non-match)

	public final static int DISPSTAT_HBLANK_EVAL_F = 0x0002; // H-Blank
																// Evaluation
																// flag (true =
																// in h-blank,
																// false =
																// outside
																// h-blank)

	public final static int DISPSTAT_VBLANK_EVAL_F = 0x0001; // V-Blank
																// Evaluation
																// flag (true =
																// in v-blank,
																// false =
																// outside
																// v-blank)

	// BG0/1/2/3 flags/masks/shifts
	public final static int BG_SCREENSIZE = 0xc000; // Screen Size

	public final static int BG_SCREENSIZE_X = 0x4000; // Screen Size

	public final static int BG_SCREENSIZE_Y = 0x8000; // Screen Size

	public final static int BG_AREAOVERFLOW = 0x2000; // Screen Base Block

	public final static int BG_SCREENBASEBLOCK = 0x1f00; // Screen Base Block

	public final static int BG_COLORMODE = 0x0080; // Color Mode

	public final static int BG_MOSAIC = 0x0040; // Mosaic

	public final static int BG_CHARBASEBLOCK = 0x000c; // Character Base Block

	public final static int BG_PRIORITY = 0x0003; // Priority

	// MOSAIC flags/masks/shifts
	public final static int MOSAIC_BG_X = 0x000f; // Mosaic BG Horizontal Size

	public final static int MOSAIC_BG_Y = 0x00f0; // Mosaic BG Vertical Size

	public final static int MOSAIC_OBJ_X = 0x0f00; // Mosaic OBJ Horizontal
													// Size

	public final static int MOSAIC_OBJ_Y = 0xf000; // Mosaic OBJ Vertical Size

	// DMA0/1/2/3 flags/masks/shifts
	public final static int DMCNT_H_ENABLE = 0x8000; // DMA Enable flag

	public final static int DMCNT_H_IRQ_REQUEST = 0x4000; // DMA Interrupt
															// Request Enable
															// flag

	public final static int DMCNT_H_DMA_START = 0x3000; // DMA Start Timing

	public final static int DMCNT_H_DMA_START_IMM = 0x0000; // DMA Start Timing
															// : Immediate

	public final static int DMCNT_H_DMA_START_VB = 0x1000; // DMA Start Timing
															// : Start in HBLANK

	public final static int DMCNT_H_DMA_START_HB = 0x2000; // DMA Start Timing
															// : Start in VBLANK

	public final static int DMCNT_H_DMA_TRANSFER32 = 0x0400; // DMA Transfer
																// Type

	public final static int DMCNT_H_DMA_REPEAT = 0x0200; // DMA Repeat flag

	public final static int DMCNT_H_DMA_SRCCTRL = 0x0180; // DMA Source
															// Address Control

	public final static int DMCNT_H_DMA_SRCCTRL_INC = 0x0000; // DMA Source
																// Address
																// Control :
																// Increment

	public final static int DMCNT_H_DMA_SRCCTRL_DEC = 0x0080; // DMA Source
																// Address
																// Control :
																// Decrement

	public final static int DMCNT_H_DMA_SRCCTRL_FIX = 0x0100; // DMA Source
																// Address
																// Control :
																// Fixed

	public final static int DMCNT_H_DMA_DSTCTRL = 0x0060; // DMA Source
															// Address Control

	public final static int DMCNT_H_DMA_DSTCTRL_INC = 0x0000; // DMA
																// Destination
																// Address
																// Control :
																// Increment

	public final static int DMCNT_H_DMA_DSTCTRL_DEC = 0x0020; // DMA
																// Destination
																// Address
																// Control :
																// Decrement

	public final static int DMCNT_H_DMA_DSTCTRL_FIX = 0x0040; // DMA
																// Destination
																// Address
																// Control :
																// Fixed

	public final static int DMCNT_H_DMA_DSTCTRL_REL = 0x0060; // DMA
																// Destination
																// Address
																// Control :
																// Increment/Reload

	// P1 flags/masks/shifts
	public final static int P1_L = 0x0200; // L button flag

	public final static int P1_R = 0x0100; // R button flag

	public final static int P1_DOWN = 0x0080; // Down button flag

	public final static int P1_UP = 0x0040; // Up button flag

	public final static int P1_LEFT = 0x0020; // Left button flag

	public final static int P1_RIGHT = 0x0010; // Right button flag

	public final static int P1_START = 0x0008; // Start button flag

	public final static int P1_SELECT = 0x0004; // Select button flag

	public final static int P1_A = 0x0002; // A button flag

	public final static int P1_B = 0x0001; // B button flag

	public final static int P1_ALL = 0x03ff; // All keys released mask

	// P1 IRQ flags/masks/shifts
	public final static int P1CNT_INTCOND = 0x8000; // Interrupt condition spec

	public final static int P1CNT_ENABLE = 0x4000; // Interrupt enable

	// IRQ Enable flags
	public final static int IRQ_GAMEPAK = 0x2000; // Game Pak interrupt
													// (DREQ/IREQ)

	public final static int IRQ_KEY = 0x1000; // Key interrupt

	public final static int IRQ_DMA0 = 0x0100; // DMA 0 interrupt

	public final static int IRQ_DMA1 = 0x0200; // DMA 1 interrupt

	public final static int IRQ_DMA2 = 0x0400; // DMA 2 interrupt

	public final static int IRQ_DMA3 = 0x0800; // DMA 3 interrupt

	public final static int IRQ_UART = 0x0080; // UART interrupt

	public final static int IRQ_TIMER0 = 0x0040; // Timer 0 interrupt

	public final static int IRQ_TIMER1 = 0x0020; // Timer 1 interrupt

	public final static int IRQ_TIMER2 = 0x0010; // Timer 2 interrupt

	public final static int IRQ_TIMER3 = 0x0008; // Timer 3 interrupt

	public final static int IRQ_VCOUNT = 0x0004; // VCOUNT interrupt

	public final static int IRQ_HBLANK = 0x0002; // HBLANK interrupt

	public final static int IRQ_VBLANK = 0x0001; // VBLANK interrupt

}
