package javagba;

public interface CoreConstants {

	// ARM7TDMI Operating Modes
	public final static int PSR_MODE_USER = 0x00000010; // Operation Mode : User
														// mode

	public final static int PSR_MODE_FIQ = 0x00000011; // Operation Mode : FIQ
														// mode

	public final static int PSR_MODE_IRQ = 0x00000012; // Operation Mode : IRQ
														// mode

	public final static int PSR_MODE_SUPERVISOR = 0x00000013; // Operation
																// Mode :
																// Supervisor
																// mode

	public final static int PSR_MODE_ABORT = 0x00000017; // Operation Mode :
															// Abort mode

	public final static int PSR_MODE_UNDEFINED = 0x0000001b; // Operation
																// Mode :
																// Undefined
																// mode

	public final static int PSR_MODE_SYSTEM = 0x0000001f; // Operation Mode :
															// System mode

	public final static int PSR_MODE = 0x0000001f; // Operation Mode :
													// Operation mode flag

	// Program Status Register Flags
	public final static int PSR_FLAG_STATE = 0x00000020; // PSR Flag : State

	public final static int PSR_FLAG_FIQDISABLE = 0x00000040; // PSR Flag :
																// FIQ Disable

	public final static int PSR_FLAG_IRQDISABLE = 0x00000080; // PSR Flag :
																// FIQ Disable

	public final static int PSR_FLAG_OVERFLOW = 0x10000000; // PSR Flag : FIQ
															// Disable

	public final static int PSR_FLAG_CARRY = 0x20000000; // PSR Flag : FIQ
															// Disable

	public final static int PSR_FLAG_ZERO = 0x40000000; // PSR Flag : FIQ
														// Disable

	public final static int PSR_FLAG_NEGATIVE = 0x80000000; // PSR Flag : FIQ
															// Disable

	public final static int PSR_SHFT_STATE = 5; // PSR Flag Shift : State

	// ARM7TDMI General Registers and Program Counter
	public final static int REG_R0 = 0x00; // ARM register R0

	public final static int REG_R1 = 0x01; // ARM register R1

	public final static int REG_R2 = 0x02; // ARM register R2

	public final static int REG_R3 = 0x03; // ARM register R3

	public final static int REG_R4 = 0x04; // ARM register R4

	public final static int REG_R5 = 0x05; // ARM register R5

	public final static int REG_R6 = 0x06; // ARM register R6

	public final static int REG_R7 = 0x07; // ARM register R7

	public final static int REG_R8 = 0x08; // ARM register R8

	public final static int REG_R9 = 0x09; // ARM register R9

	public final static int REG_R10 = 0x0a; // ARM register R10

	public final static int REG_R11 = 0x0b; // ARM register R11

	public final static int REG_R12 = 0x0c; // ARM register R12

	public final static int REG_R13 = 0x0d; // ARM register R13

	public final static int REG_R14 = 0x0e; // ARM register R14

	public final static int REG_R15 = 0x0f; // ARM register R15

	public final static int REG_SP = 0x0d; // ARM register Stack Pointer

	public final static int REG_LR = 0x0e; // ARM register Link Register

	public final static int REG_PC = 0x0f; // ARM register Program Counter

	// ARM State Program Status Registers
	public final static int REG_CPSR = 0x10; // ARM state register Current
												// Program Status Register

	public final static int REG_SPSR = 0x11; // ARM state register Saved
												// Program Status Register

	// THUMB State General Registers and Program Counter
	public final static int REG_THUMB_R0 = 0x00; // THUMB state register R0

	public final static int REG_THUMB_R1 = 0x01; // THUMB state register R1

	public final static int REG_THUMB_R2 = 0x02; // THUMB state register R2

	public final static int REG_THUMB_R3 = 0x03; // THUMB state register R3

	public final static int REG_THUMB_R4 = 0x04; // THUMB state register R4

	public final static int REG_THUMB_R5 = 0x05; // THUMB state register R5

	public final static int REG_THUMB_R6 = 0x06; // THUMB state register R6

	public final static int REG_THUMB_R7 = 0x07; // THUMB state register R7

	public final static int REG_THUMB_SP = 0x0d; // THUMB state register
													// Stack Pointer

	public final static int REG_THUMB_LR = 0x0e; // THUMB state register Link
													// Register

	public final static int REG_THUMB_PC = 0x0f; // THUMB state register
													// Program Counter

	// THUMB State Program Status Registers
	public final static int REG_THUMB_CPSR = 0x10; // ARM state register
													// Current Program Status
													// Register

	public final static int REG_THUMB_SPSR = 0x11; // ARM state register Saved
													// Program Status Register

	// ARM7TDMI Exception Vector Addresses
	public final static int EXC_ADR_RESET = 0x00000000; // Exception Vector
														// Address : Reset

	public final static int EXC_ADR_UNDEFINED = 0x00000004; // Exception Vector
															// Address :
															// Undefined
															// instruction

	public final static int EXC_ADR_SOFTWAREINT = 0x00000008; // Exception
																// Vector
																// Address :
																// Software
																// Interrupt

	public final static int EXC_ADR_ABORT_PREF = 0x0000000c; // Exception
																// Vector
																// Address :
																// Abort
																// (prefetch)

	public final static int EXC_ADR_ABORT_DATA = 0x00000010; // Exception
																// Vector
																// Address :
																// Abort (data)

	public final static int EXC_ADR_RESERVED = 0x00000014; // Exception Vector
															// Address :
															// Reserved

	public final static int EXC_ADR_IRQ = 0x00000018; // Exception Vector
														// Address : IRQ

	public final static int EXC_ADR_FIQ = 0x0000001c; // Exception Vector
														// Address : FIQ

	// ARM Condition Codes
	public final static int COND_EQ = 0x00000000; // Condition Code : Equal

	public final static int COND_NE = 0x10000000; // Condition Code : Not
													// equal

	public final static int COND_CS = 0x20000000; // Condition Code : Unsigned
													// higher or same

	public final static int COND_CC = 0x30000000; // Condition Code : Unsigned
													// lower

	public final static int COND_MI = 0x40000000; // Condition Code : Negative

	public final static int COND_PL = 0x50000000; // Condition Code : Positive
													// or zero

	public final static int COND_VS = 0x60000000; // Condition Code : Overflow

	public final static int COND_VC = 0x70000000; // Condition Code : No
													// overflow

	public final static int COND_HI = 0x80000000; // Condition Code : Unsigned
													// higher

	public final static int COND_LS = 0x90000000; // Condition Code : Unsigned
													// lower or same

	public final static int COND_GE = 0xa0000000; // Condition Code : Greater
													// than or equal

	public final static int COND_LT = 0xb0000000; // Condition Code : Less
													// than

	public final static int COND_GT = 0xc0000000; // Condition Code : Greater
													// than

	public final static int COND_LE = 0xd0000000; // Condition Code : Less
													// than or equal

	public final static int COND_AL = 0xe0000000; // Condition Code : All

	// ARM Instruction Masks/Values
	public final static int INSTR_AIM_B = 0x0e000000; // ARM Instruction Mask
														// : Branch

	public final static int INSTR_AIM_BX = 0x0ffffff0; // ARM Instruction Mask
														// : Branch and Exchange

	public final static int INSTR_AIM_DATAPROC = 0x0c000000; // ARM
																// Instruction
																// Mask : Data
																// Processing /
																// PSR Transfer

	public final static int INSTR_AIM_DATAPROCT = 0x0d900000; // ARM
																// Instruction
																// Mask : Data
																// Processing
																// Test
																// Operations

	public final static int INSTR_AIM_LDM = 0x0e000000; // ARM Instruction Mask
														// : Block Data Transfer

	public final static int INSTR_AIM_LDR = 0x0c000000; // ARM Instruction Mask
														// : Single Data
														// Transfer

	public final static int INSTR_AIM_LDRH = 0x0e000090; // ARM Instruction
															// Mask : Halfword
															// Data Transfer

	public final static int INSTR_AIM_MRS = 0x0fbf0fff; // ARM Instruction Mask
														// : Move Register to
														// PSR

	public final static int INSTR_AIM_MSR = 0x0db6f000; // ARM Instruction Mask
														// : Move PSR to
														// Register

	public final static int INSTR_AIM_MUL = 0x0fc000f0; // ARM Instruction Mask
														// : Multiply

	public final static int INSTR_AIM_MULL = 0x0f8000f0; // ARM Instruction
															// Mask : Multiply
															// Long

	public final static int INSTR_AIM_SWI = 0x0f000000; // ARM Instruction Mask
														// : Software Interrupt

	public final static int INSTR_AIM_SWP = 0x0fb00ff0; // ARM Instruction Mask
														// : Single Data Swap

	public final static int INSTR_AIM_UNDEF = 0x0e000010; // ARM Instruction
															// Mask : Undefined
															// Instruction

	public final static int INSTR_AIT_B = 0x0a000000; // ARM Instruction Test
														// : Branch

	public final static int INSTR_AIT_BX = 0x012fff10; // ARM Instruction Test
														// : Branch and Exchange

	public final static int INSTR_AIT_DATAPROC = 0x00000000; // ARM
																// Instruction
																// Test : Data
																// Processing /
																// PSR Transfer

	public final static int INSTR_AIT_DATAPROCT = 0x01100000; // ARM
																// Instruction
																// Test : Data
																// Processing
																// Test
																// Operations

	public final static int INSTR_AIT_LDM = 0x08000000; // ARM Instruction Test
														// : Block Data Transfer

	public final static int INSTR_AIT_LDR = 0x04000000; // ARM Instruction Test
														// : Single Data
														// Transfer

	public final static int INSTR_AIT_LDRH = 0x00000090; // ARM Instruction
															// Test : Halfword
															// Data Transfer

	public final static int INSTR_AIT_MRS = 0x010f0000; // ARM Instruction Test
														// : Move Register to
														// PSR

	public final static int INSTR_AIT_MSR = 0x0120f000; // ARM Instruction Test
														// : Move PSR to
														// Register

	public final static int INSTR_AIT_MUL = 0x00000090; // ARM Instruction Test
														// : Multiply

	public final static int INSTR_AIT_MULL = 0x00800090; // ARM Instruction
															// Test : Multiply
															// Long

	public final static int INSTR_AIT_SWI = 0x0f000000; // ARM Instruction Test
														// : Software Interrupt

	public final static int INSTR_AIT_SWP = 0x01000090; // ARM Instruction Test
														// : Single Data Swap

	public final static int INSTR_AIT_UNDEF = 0x06000010; // ARM Instruction
															// Test : Undefined
															// Instruction

	public final static int INSTR_MASK_COND = 0xf0000000; // ARM Instruction
															// Mask : Condition
															// mask

	public final static int INSTR_MASK_R_00_03 = 0x0000000f; // ARM
																// Instruction
																// Mask :
																// Register
																// index (bits
																// 0-3)

	public final static int INSTR_MASK_R_08_11 = 0x00000f00; // ARM
																// Instruction
																// Mask :
																// Register
																// index (bits
																// 8-11)

	public final static int INSTR_MASK_R_12_15 = 0x0000f000; // ARM
																// Instruction
																// Mask :
																// Register
																// index (bits
																// 12-15)

	public final static int INSTR_MASK_R_16_19 = 0x000f0000; // ARM
																// Instruction
																// Mask :
																// Register
																// index (bits
																// 16-19)

	public final static int INSTR_SHFT_COND = 28; // ARM Instruction Shift :
													// Condition

	public final static int INSTR_SHFT_R_00_03 = 0; // ARM Instruction Shift :
													// Register index (bits 0-3)

	public final static int INSTR_SHFT_R_08_11 = 8; // ARM Instruction Shift :
													// Register index (bits
													// 8-11)

	public final static int INSTR_SHFT_R_12_15 = 12; // ARM Instruction Shift
														// : Register index
														// (bits 12-15)

	public final static int INSTR_SHFT_R_16_19 = 16; // ARM Instruction Shift
														// : Register index
														// (bits 16-19)

	// ARM Instruction Specific Masks/Values
	public final static int INSTR_B_LINKBIT = 0x01000000; // ARM Branch
															// Instruction :
															// Link bit

	public final static int INSTR_B_OFFSET = 0x00ffffff; // ARM Branch
															// Instruction :
															// Branch offset

	public final static int INSTR_DP_OP_MASK = 0x01e00000; // ARM Data
															// Processing
															// Instruction :
															// OpCode mask

	public final static int INSTR_DP_OP_AND = 0x00000000; // ARM Data
															// Processing
															// Instruction : AND
															// operation

	public final static int INSTR_DP_OP_EOR = 0x00200000; // ARM Data
															// Processing
															// Instruction : EOR
															// operation

	public final static int INSTR_DP_OP_SUB = 0x00400000; // ARM Data
															// Processing
															// Instruction : SUB
															// operation

	public final static int INSTR_DP_OP_RSB = 0x00600000; // ARM Data
															// Processing
															// Instruction : RSB
															// operation

	public final static int INSTR_DP_OP_ADD = 0x00800000; // ARM Data
															// Processing
															// Instruction : ADD
															// operation

	public final static int INSTR_DP_OP_ADC = 0x00a00000; // ARM Data
															// Processing
															// Instruction : ADC
															// operation

	public final static int INSTR_DP_OP_SBC = 0x00c00000; // ARM Data
															// Processing
															// Instruction : SBC
															// operation

	public final static int INSTR_DP_OP_RSC = 0x00e00000; // ARM Data
															// Processing
															// Instruction : RSC
															// operation

	public final static int INSTR_DP_OP_TST = 0x01000000; // ARM Data
															// Processing
															// Instruction : TST
															// operation

	public final static int INSTR_DP_OP_TEQ = 0x01200000; // ARM Data
															// Processing
															// Instruction : TEQ
															// operation

	public final static int INSTR_DP_OP_CMP = 0x01400000; // ARM Data
															// Processing
															// Instruction : CMP
															// operation

	public final static int INSTR_DP_OP_CMN = 0x01600000; // ARM Data
															// Processing
															// Instruction : CMN
															// operation

	public final static int INSTR_DP_OP_ORR = 0x01800000; // ARM Data
															// Processing
															// Instruction : ORR
															// operation

	public final static int INSTR_DP_OP_MOV = 0x01a00000; // ARM Data
															// Processing
															// Instruction : MOV
															// operation

	public final static int INSTR_DP_OP_BIC = 0x01c00000; // ARM Data
															// Processing
															// Instruction : BIC
															// operation

	public final static int INSTR_DP_OP_MVN = 0x01e00000; // ARM Data
															// Processing
															// Instruction : MVN
															// operation

	public final static int INSTR_DP_SETFLAGS = 0x00100000; // ARM Data
															// Processing
															// Instruction : Set
															// PSR flags flag

	public final static int INSTR_DP_IMMEDIATE = 0x02000000; // ARM Data
																// Processing
																// Instruction :
																// Immediate
																// operand flag

	public final static int INSTR_DP_IMM_VALUE = 0x000000ff; // ARM Data
																// Processing
																// Instruction :
																// Immediate
																// operand value

	public final static int INSTR_DP_IMM_ROTATE = 0x00000f00; // ARM Data
																// Processing
																// Instruction :
																// Immediate
																// operand
																// rotate value

	public final static int INSTR_DP_REG_RM = 0x0000000f; // ARM Data
															// Processing
															// Instruction :
															// Register operand
															// index

	public final static int INSTR_DP_REG_SHIFT = 0x00000ff0; // ARM Data
																// Processing
																// Instruction :
																// Register
																// operand shift

	public final static int INSTR_LDM_INDEXING = 0x01000000; // ARM Block
																// Data Transfer
																// Instructio :
																// Indexing mode
																// flag

	public final static int INSTR_LDM_UP = 0x00800000; // ARM Block Data
														// Transfer Instructio :
														// Up/down flag

	public final static int INSTR_LDM_PSRFORCE = 0x00400000; // ARM Block
																// Data Transfer
																// Instructio :
																// load PSR and
																// force user
																// mode flag

	public final static int INSTR_LDM_WRITEBACK = 0x00200000; // ARM Block
																// Data Transfer
																// Instructio :
																// Writeback
																// flag

	public final static int INSTR_LDM_LOAD = 0x00100000; // ARM Block Data
															// Transfer
															// Instructio :
															// Load/write flag

	public final static int INSTR_LDR_IMMEDIATE = 0x02000000; // ARM Single
																// Data Transfer
																// Instructio :
																// Immediate
																// value flag

	public final static int INSTR_LDR_INDEXING = 0x01000000; // ARM Single
																// Data Transfer
																// Instructio :
																// Indexing mode
																// flag

	public final static int INSTR_LDR_UP = 0x00800000; // ARM Single Data
														// Transfer Instructio :
														// Up/down flag

	public final static int INSTR_LDR_BYTE = 0x00400000; // ARM Single Data
															// Transfer
															// Instructio :
															// Byte/word flag

	public final static int INSTR_LDR_WRITEBACK = 0x00200000; // ARM Single
																// Data Transfer
																// Instructio :
																// Writeback
																// flag

	public final static int INSTR_LDR_LOAD = 0x00100000; // ARM Single Data
															// Transfer
															// Instructio :
															// Load/write flag

	public final static int INSTR_LDR_OFFSET = 0x00000fff; // ARM Single Data
															// Transfer
															// Instructio :
															// Offset mask

	public final static int INSTR_LDR_SHIFTREG = 0x0000000f; // ARM Single
																// Data Transfer
																// Instructio :
																// Shift
																// register mask

	public final static int INSTR_LDRH_IMMEDIATE = 0x00400000; // ARM Halfword
																// Data Transfer
																// Instructio :
																// Immediate
																// value flag

	public final static int INSTR_LDRH_OFFSET_H = 0x00000f00; // ARM Halfword
																// Data Transfer
																// Instructio :
																// Offset mask
																// (high nibble)

	public final static int INSTR_LDRH_OFFSET_L = 0x0000000f; // ARM Halfword
																// Data Transfer
																// Instructio :
																// Offset mask
																// (low nibble)

	public final static int INSTR_LDRH_SIGNED = 0x00000040; // ARM Halfword Data
															// Transfer
															// Instructio :
															// Signed data
															// transfer flag

	public final static int INSTR_LDRH_HALFWORD = 0x00000020; // ARM Halfword
																// Data Transfer
																// Instructio :
																// Halfword data
																// transfer flag

	public final static int INSTR_MRS_SPSR_FLAG = 0x00400000; // ARM PSR
																// Transfer
																// Instruction :
																// CPSR/SPSR
																// flag

	public final static int INSTR_MSR_SPSR_FLAG = 0x00400000; // ARM PSR
																// Transfer
																// Instruction :
																// CPSR/SPSR
																// flag

	public final static int INSTR_MSR_TYPE_FLAG = 0x00010000; // ARM PSR
																// Transfer
																// Instruction :
																// MRS type flag

	public final static int INSTR_MSR_SRC_FLAG = 0x02000000; // ARM PSR
																// Transfer
																// Instruction :
																// MRS type flag

	public final static int INSTR_MSR_IMM_VALUE = 0x000000ff; // ARM PSR
																// Transfer
																// Instruction :
																// Immediate
																// operand value

	public final static int INSTR_MSR_IMM_ROTATE = 0x00000f00; // ARM PSR
																// Transfer
																// Instruction :
																// Immediate
																// operand
																// rotate value

	public final static int INSTR_MUL_COND_FLAG = 0x00100000; // ARM Multiply
																// Instruction :
																// Set condition
																// codes flag

	public final static int INSTR_MUL_ACC_FLAG = 0x00200000; // ARM Multiply
																// Instruction :
																// Accumulate
																// flag

	public final static int INSTR_MULL_UNSIGNED = 0x00400000; // ARM Multiply
																// Instruction :
																// Accumulate
																// flag

	public final static int INSTR_MULL_ACCUM = 0x00200000; // ARM Multiply
															// Instruction :
															// Accumulate flag

	public final static int INSTR_MULL_COND = 0x00100000; // ARM Multiply
															// Instruction :
															// Accumulate flag

	public final static int INSTR_SWP_BYTE = 0x00400000; // ARM Single Data
															// Swap Instruction
															// : Byte swap flag

	// ARM Barrel Shifter Constants
	public final static int SHIFT_OP_MASK = 0x00000001; // ARM Data Processing
														// Shift : Operation
														// mask

	public final static int SHIFT_TYPE = 0x00000006; // ARM Data Processing
														// Shift : Shift type

	public final static int SHIFT_AMOUNT_VALUE = 0x000000f8; // ARM Data
																// Processing
																// Shift :
																// Immediate
																// shift value
																// mask

	public final static int SHIFT_AMOUNT_SHIFT = 0x00000003; // ARM Data
																// Processing
																// Shift :
																// Immediate
																// shift value
																// mask shift

	public final static int SHIFT_REG_INDEX = 0x000000f0; // ARM Data
															// Processing Shift
															// : Register
															// specified shift
															// index

	public final static int SHIFT_REG_SHIFT = 0x00000004; // ARM Data
															// Processing Shift
															// : Register
															// specified shift
															// index shift

	public final static int SHIFT_LSL = 0x00000000; // ARM Data Processing Shift
													// : Logical shift left

	public final static int SHIFT_LSR = 0x00000002; // ARM Data Processing Shift
													// : Logical shift right

	public final static int SHIFT_ASR = 0x00000004; // ARM Data Processing Shift
													// : Arithemtic shift right

	public final static int SHIFT_ROR = 0x00000006; // ARM Data Processing Shift
													// : Rotate right

	// THUMB Instruction Masks/Values
	public final static int INSTR_TIM_LSL = 0xf800; // THUMB Instruction Mask :
													// Logical Shift Left

	public final static int INSTR_TIM_LSR = 0xf800; // THUMB Instruction Mask :
													// Logical Shift Right

	public final static int INSTR_TIM_ASR = 0xf800; // THUMB Instruction Mask :
													// Arithmetic Shift Right

	public final static int INSTR_TIM_ADD = 0xf800; // THUMB Instruction Mask :
													// Add/Substract

	public final static int INSTR_TIM_MOV = 0xe000; // THUMB Instruction Mask :
													// Move/Compare/Add/Substract
													// immediate

	public final static int INSTR_TIM_ALU = 0xfc00; // THUMB Instruction Mask :
													// ALU operations

	public final static int INSTR_TIM_HIREG = 0xfc00; // THUMB Instruction
														// Mask : Hi Register
														// Operations

	public final static int INSTR_TIM_PCLOAD = 0xf800; // THUMB Instruction
														// Mask : PC Relative
														// Load

	public final static int INSTR_TIM_LDR = 0xf200; // THUMB Instruction Mask :
													// Load/Store with register
													// offset

	public final static int INSTR_TIM_LDRHS = 0xf200; // THUMB Instruction
														// Mask : Load/Store
														// sign extended
														// byte/halfword

	public final static int INSTR_TIM_LDRI = 0xe000; // THUMB Instruction
														// Mask : Load/Store
														// with immediate offset

	public final static int INSTR_TIM_LDRH = 0xf000; // THUMB Instruction
														// Mask : Load/Store
														// halfword

	public final static int INSTR_TIM_SPLOAD = 0xf000; // THUMB Instruction
														// Mask : SP Relative
														// Load

	public final static int INSTR_TIM_LOADADDR = 0xf000; // THUMB Instruction
															// Mask : Load
															// address

	public final static int INSTR_TIM_SPADD = 0xff00; // THUMB Instruction
														// Mask : Add offset to
														// stack pointer

	public final static int INSTR_TIM_PUSHPOP = 0xf600; // THUMB Instruction
														// Mask : Push/Pop
														// registers

	public final static int INSTR_TIM_MULTLS = 0xf000; // THUMB Instruction
														// Mask : Multiple
														// load/store

	public final static int INSTR_TIM_CBRANCH = 0xf000; // THUMB Instruction
														// Mask : Conditional
														// branch

	public final static int INSTR_TIM_SWI = 0xff00; // THUMB Instruction Mask :
													// Software interrupt

	public final static int INSTR_TIM_BRANCH = 0xf800; // THUMB Instruction
														// Mask : Branch

	public final static int INSTR_TIM_LBRANCH = 0xf000; // THUMB Instruction
														// Mask : Branch with
														// link

	public final static int INSTR_TIT_LSL = 0x0000; // THUMB Instruction Test :
													// Logical Shift Left

	public final static int INSTR_TIT_LSR = 0x0800; // THUMB Instruction Test :
													// Logical Shift Right

	public final static int INSTR_TIT_ASR = 0x1000; // THUMB Instruction Test :
													// Arithmetic Shift Right

	public final static int INSTR_TIT_ADD = 0x1800; // THUMB Instruction Test :
													// Add/Substract

	public final static int INSTR_TIT_MOV = 0x2000; // THUMB Instruction Test :
													// Move/Compare/Add/Substract
													// immediate

	public final static int INSTR_TIT_ALU = 0x4000; // THUMB Instruction Test :
													// ALU operations

	public final static int INSTR_TIT_HIREG = 0x4400; // THUMB Instruction
														// Test : Hi Register
														// Operations

	public final static int INSTR_TIT_PCLOAD = 0x4800; // THUMB Instruction
														// Test : PC Relative
														// Load

	public final static int INSTR_TIT_LDR = 0x5000; // THUMB Instruction Test :
													// Load/Store with register
													// offset

	public final static int INSTR_TIT_LDRHS = 0x5200; // THUMB Instruction
														// Test : Load/Store
														// sign extended
														// byte/halfword

	public final static int INSTR_TIT_LDRI = 0x6000; // THUMB Instruction
														// Test : Load/Store
														// with immediate offset

	public final static int INSTR_TIT_LDRH = 0x8000; // THUMB Instruction
														// Test : Load/Store
														// halfword

	public final static int INSTR_TIT_SPLOAD = 0x9000; // THUMB Instruction
														// Test : SP Relative
														// Load

	public final static int INSTR_TIT_LOADADDR = 0xa000; // THUMB Instruction
															// Test : Load
															// address

	public final static int INSTR_TIT_SPADD = 0xb000; // THUMB Instruction
														// Test : Add offset to
														// stack pointer

	public final static int INSTR_TIT_PUSHPOP = 0xb400; // THUMB Instruction
														// Test : Push/Pop
														// registers

	public final static int INSTR_TIT_MULTLS = 0xc000; // THUMB Instruction
														// Test : Multiple
														// load/store

	public final static int INSTR_TIT_CBRANCH = 0xd000; // THUMB Instruction
														// Test : Conditional
														// branch

	public final static int INSTR_TIT_SWI = 0xdf00; // THUMB Instruction Test :
													// Software interrupt

	public final static int INSTR_TIT_BRANCH = 0xe000; // THUMB Instruction
														// Mask : Branch

	public final static int INSTR_TIT_LBRANCH = 0xf000; // THUMB Instruction
														// Mask : Branch with
														// link

	public final static int INSTR_MASK_R_00_02 = 0x0007; // THUMB Instruction
															// Mask : Register
															// index (bits 0-2)

	public final static int INSTR_MASK_R_03_05 = 0x0038; // THUMB Instruction
															// Mask : Register
															// index (bits 3-5)

	public final static int INSTR_MASK_R_06_08 = 0x01c0; // THUMB Instruction
															// Mask : Register
															// index (bits 6-8)

	public final static int INSTR_MASK_R_08_10 = 0x0700; // THUMB Instruction
															// Mask : Register
															// index (bits 8-10)

	public final static int INSTR_SHFT_R_00_02 = 0; // THUMB Instruction Shift :
													// Register index (bits 0-2)

	public final static int INSTR_SHFT_R_03_05 = 3; // THUMB Instruction Shift :
													// Register index (bits 3-5)

	public final static int INSTR_SHFT_R_06_08 = 6; // THUMB Instruction Shift :
													// Register index (bits 6-8)

	public final static int INSTR_SHFT_R_08_10 = 8; // THUMB Instruction Shift :
													// Register index (bits
													// 8-10)

	// THUMB Instruction Specific Masks/Values
	public final static int INSTR_LSL_OFFSET = 0x07c0; // THUMB Move Shift
														// Register : Offset
														// mask

	public final static int INSTR_ADD_IMMEDIATE = 0x0400; // THUMB
															// Add/Substract :
															// Immediate flag

	public final static int INSTR_ADD_OP = 0x0200; // THUMB Add/Substract : Op
													// flag

	public final static int INSTR_ADD_OFFSET = 0x01c0; // THUMB Add/Substract :
														// Rn/Offset mask

	public final static int INSTR_MOV_OP = 0x1800; // THUMB
													// Move/compare/add/sub :
													// Operation mask

	public final static int INSTR_MOV_OP_MOV = 0x0000; // THUMB
														// Move/compare/add/sub
														// : Operation move

	public final static int INSTR_MOV_OP_CMP = 0x0800; // THUMB
														// Move/compare/add/sub
														// : Operation compare

	public final static int INSTR_MOV_OP_ADD = 0x1000; // THUMB
														// Move/compare/add/sub
														// : Operation addition

	public final static int INSTR_MOV_OP_SUB = 0x1800; // THUMB
														// Move/compare/add/sub
														// : Operation substract

	public final static int INSTR_MOV_OFFSET = 0x00ff; // THUMB
														// Move/compare/add/sub
														// : Offset mask

	public final static int INSTR_ALU_OP = 0x03c0; // THUMB ALU operations : Op
													// mask

	public final static int INSTR_ALU_OP_AND = 0x0000; // THUMB ALU operations
														// : AND

	public final static int INSTR_ALU_OP_EOR = 0x0040; // THUMB ALU operations
														// : EOR

	public final static int INSTR_ALU_OP_LSL = 0x0080; // THUMB ALU operations
														// : LSL

	public final static int INSTR_ALU_OP_LSR = 0x00c0; // THUMB ALU operations
														// : LSR

	public final static int INSTR_ALU_OP_ASR = 0x0100; // THUMB ALU operations
														// : ASR

	public final static int INSTR_ALU_OP_ADC = 0x0140; // THUMB ALU operations
														// : ADC

	public final static int INSTR_ALU_OP_SBC = 0x0180; // THUMB ALU operations
														// : SBC

	public final static int INSTR_ALU_OP_ROR = 0x01c0; // THUMB ALU operations
														// : ROR

	public final static int INSTR_ALU_OP_TST = 0x0200; // THUMB ALU operations
														// : TST

	public final static int INSTR_ALU_OP_NEG = 0x0240; // THUMB ALU operations
														// : NEG

	public final static int INSTR_ALU_OP_CMP = 0x0280; // THUMB ALU operations
														// : CMP

	public final static int INSTR_ALU_OP_CMN = 0x02c0; // THUMB ALU operations
														// : CMN

	public final static int INSTR_ALU_OP_ORR = 0x0300; // THUMB ALU operations
														// : ORR

	public final static int INSTR_ALU_OP_MUL = 0x0340; // THUMB ALU operations
														// : MUL

	public final static int INSTR_ALU_OP_BIC = 0x0380; // THUMB ALU operations
														// : BIC

	public final static int INSTR_ALU_OP_MVN = 0x03c0; // THUMB ALU operations
														// : MVN

	public final static int INSTR_HIREG_OP = 0x0300; // THUMB HIREG
														// operations : Op mask

	public final static int INSTR_HIREG_OP_ADD = 0x0000; // THUMB HIREG
															// operations : Add

	public final static int INSTR_HIREG_OP_CMP = 0x0100; // THUMB HIREG
															// operations :
															// Compare

	public final static int INSTR_HIREG_OP_MOV = 0x0200; // THUMB HIREG
															// operations : Move

	public final static int INSTR_HIREG_OP_BX = 0x0300; // THUMB HIREG
														// operations : Branch
														// Exchange

	public final static int INSTR_HIREG_OP_H1 = 0x0080; // THUMB HIREG
														// operations : H1 flag

	public final static int INSTR_HIREG_OP_H2 = 0x0040; // THUMB HIREG
														// operations : H2 flag

	public final static int INSTR_PCLOAD_IMM = 0x00ff; // THUMB PC relative
														// load : Immediate
														// value mask

	public final static int INSTR_TLDR_LOAD = 0x0800; // THUMB

	public final static int INSTR_TLDR_BYTE = 0x0400; // THUMB

	public final static int INSTR_LDRHS_HALFWORD = 0x0800; // THUMB

	public final static int INSTR_LDRHS_SIGN = 0x0400; // THUMB

	public final static int INSTR_TLDRH_LOAD = 0x0800; // THUMB

	public final static int INSTR_TLDRH_OFFSET = 0x07c0; // THUMB

	public final static int INSTR_LDRI_BYTE = 0x1000; // THUMB

	public final static int INSTR_LDRI_LOAD = 0x0800; // THUMB

	public final static int INSTR_LDRI_OFFSET = 0x07c0; // THUMB

	public final static int INSTR_LDRH_LOAD = 0x0800; // THUMB

	public final static int INSTR_LDRH_OFFSET = 0x07c0; // THUMB

	public final static int INSTR_SPLOAD_IMM = 0x00ff; // THUMB

	public final static int INSTR_SPLOAD_LOAD = 0x0800; // THUMB

	public final static int INSTR_LOADADDR_SP = 0x0800; // THUMB

	public final static int INSTR_LOADADDR_OFFSET = 0x00ff; // THUMB

	public final static int INSTR_SPADD_SIGN = 0x0080; // THUMB

	public final static int INSTR_SPADD_OFFSET = 0x007f; // THUMB

	public final static int INSTR_PUSHPOP_LOAD = 0x0800; // THUMB

	public final static int INSTR_PUSHPOP_PCLR = 0x0100; // THUMB

	public final static int INSTR_MULTLS_LOAD = 0x0800; // THUMB

	public final static int INSTR_CBRANCH_COND = 0x0f00; // THUMB

	public final static int INSTR_CBRANCH_OFFSET = 0x00ff; // THUMB

	public final static int INSTR_BRANCH_OFFSET = 0x07ff; // THUMB

	public final static int INSTR_LBRANCH_OFFSET = 0x07ff; // THUMB

	public final static int INSTR_LBRANCH_HI = 0x0800; // THUMB

}
