package javagba.video;

public class Mode1ScanlineRenderer extends AbstractScanlineRenderer {
	public Mode1ScanlineRenderer(Renderer renderer) {

		// Call inherited constructor
		super(renderer);
	}

	public void renderScanline(int index) {

		// Render background scanline
		renderBackgroundScanline(index);

		// Get BG Enabled flags
		boolean BG0ENABLED = (renderer.registers.getRegister(REG_DISPCNT) & DISPCNT_DISP_BG0_F) != 0;
		boolean BG1ENABLED = (renderer.registers.getRegister(REG_DISPCNT) & DISPCNT_DISP_BG1_F) != 0;
		boolean BG2ENABLED = (renderer.registers.getRegister(REG_DISPCNT) & DISPCNT_DISP_BG2_F) != 0;
		boolean OBJENABLED = (renderer.registers.getRegister(REG_DISPCNT) & DISPCNT_DISP_OBJ_F) != 0;

		// Get BG control register values
		int BG0CNT = renderer.registers.getRegister(REG_BG0CNT);
		int BG1CNT = renderer.registers.getRegister(REG_BG1CNT);
		int BG2CNT = renderer.registers.getRegister(REG_BG2CNT);

		// Get BG priorities
		int BG0PRIORITY = BG0CNT & BG_PRIORITY;
		int BG1PRIORITY = BG1CNT & BG_PRIORITY;
		int BG2PRIORITY = BG2CNT & BG_PRIORITY;

		// Render background
		for (int priority = 3; priority >= 0; priority--) {

			if (BG2ENABLED && (BG2PRIORITY == priority))
				renderScaleRotateModeLine(index, 2, BG2CNT);
			if (BG1ENABLED && (BG1PRIORITY == priority))
				renderTextModeLine(index, 1, BG1CNT);
			if (BG0ENABLED && (BG0PRIORITY == priority))
				renderTextModeLine(index, 0, BG0CNT);
			if (OBJENABLED)
				renderSpriteLine(index, priority, 0x00010000);
		}
	}
}
