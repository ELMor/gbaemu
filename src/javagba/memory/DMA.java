package javagba.memory;

import javagba.IORegConstants;

public class DMA implements IORegConstants {

	public Memory memory; // Memory manager reference

	public DMA(Memory memory) {

		// Assign memory
		this.memory = memory;
	}

	public void doTransfer(int dmaIndex) {

		// Get register memory bank reference
		MemoryRegisters registers = (MemoryRegisters) memory.banks[Memory.MEM_IOREG];

		// Get source address
		int addrSrcLo = registers.getRegister(REG_DM0SAD_L + (dmaIndex * 0x0c));
		int addrSrcHi = registers.getRegister(REG_DM0SAD_H + (dmaIndex * 0x0c));
		int addrSrc = ((addrSrcHi << 16) & 0xffff0000)
				| (addrSrcLo & 0x0000ffff);

		// Get destination address
		int addrDstLo = registers.getRegister(REG_DM0DAD_L + (dmaIndex * 0x0c));
		int addrDstHi = registers.getRegister(REG_DM0DAD_H + (dmaIndex * 0x0c));
		int addrDst = ((addrDstHi << 16) & 0xffff0000)
				| (addrDstLo & 0x0000ffff);

		// Get size of transfer unit
		int size = (registers.getRegister(REG_DM0CNT_H + (dmaIndex * 0x0c)) & DMCNT_H_DMA_TRANSFER32) != 0 ? 4
				: 2;

		// Get amount of data to be transfered
		int amount = (registers.getRegister(REG_DM0CNT_L + (dmaIndex * 0x0c)) & 0x0000ffff);

		// Handle wrap around
		if (amount == 0)
			amount = 0x00004000;

		// Store initial destination address
		int oldAddrDst = addrDst;

		// Get source increment value
		int addrSrcInc = 0;
		switch (registers.getRegister(REG_DM0CNT_H + (dmaIndex * 0x0c))
				& DMCNT_H_DMA_SRCCTRL) {

		case DMCNT_H_DMA_SRCCTRL_INC:
			addrSrcInc = size;
			break;
		case DMCNT_H_DMA_SRCCTRL_DEC:
			addrSrcInc = -size;
			break;
		}

		// Get destination increment value
		int addrDstInc = 0;
		switch (registers.getRegister(REG_DM0CNT_H + (dmaIndex * 0x0c))
				& DMCNT_H_DMA_DSTCTRL) {

		case DMCNT_H_DMA_DSTCTRL_INC:
		case DMCNT_H_DMA_DSTCTRL_REL:
			addrDstInc = size;
			break;
		case DMCNT_H_DMA_DSTCTRL_DEC:
			addrDstInc = -size;
			break;
		}

		// Handle 16 bit transfer
		if (size == 2)
			for (int i = 0; i < amount; i++, addrSrc += addrSrcInc, addrDst += addrDstInc)
				memory.writeHalfWord(memory.readHalfWord(addrSrc), addrDst);

		// Handle 32 bit transfer
		else
			for (int i = 0; i < amount; i++, addrSrc += addrSrcInc, addrDst += addrDstInc)
				memory.writeWord(memory.readWord(addrSrc), addrDst);

		// Restore destination address if needed
		if ((registers.getRegister(REG_DM0CNT_H + (dmaIndex * 0x0c)) & DMCNT_H_DMA_DSTCTRL) == DMCNT_H_DMA_DSTCTRL_REL)
			addrDst = oldAddrDst;

		// Store source and destination
		registers.setRegister((short) (addrSrc & 0xffff), REG_DM0SAD_L
				+ (dmaIndex * 0x0c));
		registers.setRegister((short) ((addrSrc >> 16) & 0x07ff), REG_DM0SAD_H
				+ (dmaIndex * 0x0c));
		registers.setRegister((short) (addrDst & 0xffff), REG_DM0DAD_L
				+ (dmaIndex * 0x0c));
		registers.setRegister((short) ((addrDst >> 16) & 0x07ff), REG_DM0DAD_H
				+ (dmaIndex * 0x0c));
	}

	public void handleRegisterWrite(short value, int address) {

		// Get DMA index
		int dmaIndex = (address - REG_DM0SAD_L) / 0x0c;
		int dmaRegIndex = (address - REG_DM0SAD_L) % 0x0c;

		// Handle control register writes
		if (dmaRegIndex == 0x0a) {

			// Get enable flag
			boolean dmaEnabled = (value & DMCNT_H_ENABLE) != 0;

			// Handle DMA transfer if needed
			if (dmaEnabled) {

				// Get start mode
				int dmaStartMode = value & DMCNT_H_DMA_START;

				// If immediate start transfer now
				if (dmaStartMode == DMCNT_H_DMA_START_IMM)
					doTransfer(dmaIndex);

				// Reset enable flag if needed
				if ((value & DMCNT_H_DMA_REPEAT) == 0)
					memory.banks[Memory.MEM_IOREG].writeHalfWord(
							(short) (value & ~DMCNT_H_ENABLE), address);
			}
		}
	}
}
