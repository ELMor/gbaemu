package javagba;

import java.applet.Applet;
import java.awt.Color;

import javagba.core.Core;
import javagba.memory.Memory;
import javagba.memory.MemoryROM;
import javagba.video.Renderer;
import javagba.video.Viewport;

public class EmulatorApplet extends Applet implements Runnable {

	private Core core; // ARM7TDMI Core reference

	private Memory memory; // Memory Manager reference

	private Renderer renderer; // Renderer reference

	private Viewport viewport; // Emulator viewport reference

	private Thread thread; // Thread reference

	public EmulatorApplet() {

		// Set file load mode
		MemoryROM.mode = MemoryROM.MODE_URL;
	}

	public void coreRun() {

		// Start
		if (thread == null) {

			// Start continuous execution
			thread = new Thread("Debugger Execution Thread") {

				// Run method
				// @Override ELM:Estaba sin comentar
				public void run() {

					// Execution loop
					while (this.isAlive()) {

						// Give other threads time
						// Thread.yield();

						// Execute next instruction
						coreStep();
					}
				}
			};

			// Start thread
			thread.start();
		}
	}

	public void coreStep() {

		// Get current cycle count
		int cycleCount = core.cycleCount;

		// Execute next instruction
		core.executeNextInstruction();

		// Pass passed cycles to renderer
		renderer.addCycles(core.cycleCount - cycleCount);
	}

	public void init() {

		// Initialise core and memory
		memory = new Memory("/gba.bios", null);
		core = new Core(memory);
		renderer = new Renderer(memory);
		viewport = new Viewport(renderer);

		// Initialise applet surface
		setLayout(null);
		setBackground(Color.black);

		// Add viewport
		viewport.setBounds(0, 0, 244, 164);
		add(viewport);

		// Assign viewport
		renderer.setViewport(viewport);

		// Load bios and rom
		loadBios(getParameter("BIOSFILE"));
		try {
			System.out.println(getCodeBase() + getParameter("ROMFILE"));
			loadRom(getCodeBase() + getParameter("ROMFILE"));
			// Start
			start();
		} catch (Exception e) {

			System.out.println(e);
		}

	}

	public void loadBios(String fileName) {

		// Load bios rom
		memory.loadBios(fileName);
	}

	public void loadRom(String fileName) throws Exception {

		// Load rom
		memory.loadRom(fileName);
	}

	public void run() {

		// Execution loop
		while (thread.isAlive()) {

			// Give other threads time
			// Thread.yield();

			// Execute next instruction
			coreStep();
		}
	}

	public void start() {

		if (thread == null) {

			thread = new Thread(this);
			thread.start();
		}
	}

	public void stop() {

		if (thread != null) {

			thread = null;
		}
	}
}
